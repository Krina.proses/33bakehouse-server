# project-template

```
├── src
│   ├── config
│   ├── db
│   │   ├── core
│   │   ├── migrations
│   │   └── seeder-data
│   ├── middlewares
│   ├── routes
│   │   └── user
│   │       ├── permission.api.ts
│   │       ├── user-api.ts
│   │       ├── user-handler.ts
│   │       └── user-model.ts
│   ├── services
│   ├── template
│   ├── utils
│   └── validations
│   ├── app.ts
│   ├── environment.ts
│   ├── server.ts
```
# generate folder and files for api
use ``npm run create-dir <folder_name>`` to create the skeleton folder for api

example:
``npm run create-dir hello`` will create
```
│   ├── routes
│   │   └── hello
│   │       ├── hello-api.ts
│   │       ├── hello-handler.ts
│   │       └── hello-model.ts
```
