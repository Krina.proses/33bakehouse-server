import { Request, Response } from "express";
import fs from "fs";
// const multer = require("multer");
import multer, { diskStorage } from "multer";

import configs from "../config/config";
import environment from "../environment";
const config = (configs as { [key: string]: any })[environment];

const removeInitSlash = (str: string) => {
  return str.substring(1);
};

const images = "/public/cluster";
const excelFiles = "/excelFiles";
const category = "categoryImage";
const Items = "itemImage";
const attributeImageFile = "attributeImage"
const itemImageFile = "itemImageFile"

const getFileStorage = (path: string) => {
  return diskStorage({
    destination: function (req: Request, file: any, cb: any) {
      const uploadPath = `/${config.publicPath}/${path}`;
      if (!fs.existsSync(`.${uploadPath}`)) {
        fs.mkdirSync(`.${uploadPath}`);
      }
      cb(null, process.cwd() + uploadPath);
    },
    filename: function (req: Request, file: any, cb: any) {
      var d = new Date();
      cb(null, `${Date.now()}-${file.originalname}`);
    },
  });
};

// const imageFilter = function (req: Request, file: any, cb: any) {
//   // accept image only
//   if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/)) {
//     return cb(new Error("Only image files are allowed!"), false);
//   }
//   cb(null, true);
// };

// const audioFilter = function (req: any, file: any, cb: any) {
//   // accept audio only
//   if (!file.originalname.match(/\.(mp3|wav|ogg)$/)) {
//     return cb(new Error("Only valid audio files are allowed!"), false);
//   }
//   cb(null, true);
// };

// const blobFilter = function (req: any, file: any, cb: any) {
//   // accept audio only
//   if (file.originalname.match('blob')) {
//     return cb(null, false);
//   }
//   cb(null, true);
// };

const excelFilter = function (req: Request, file: any, cb: any) {
  // accept image only
  if (!file.originalname.match(/\.(xlsx)$/)) {
    return cb(new Error("Only excel files are allowed!"), false);
  }
  cb(null, true);
};

const imageFilter = function (req: Request, file: any, cb: any) {
  // accept image only
  if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/)) {
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
};

// export const imageUpload = multer({
//   storage: getFileStorage(images),
//   fileFilter: imageFilter,
// });

export const excelUpload = multer({
  storage: getFileStorage(excelFiles),
  fileFilter: excelFilter,
});

export const Category = multer({
  storage: getFileStorage(category),
  fileFilter: imageFilter,
});

export const ItemImage = multer({
  storage: getFileStorage(Items),
  fileFilter: imageFilter,
});

export const attributeImages = multer({
  storage: getFileStorage(attributeImageFile),
  // fileFilter: imageFilter,
})

export const itemImages = multer({
  storage: getFileStorage(itemImageFile),
  fileFilter: imageFilter,
})
