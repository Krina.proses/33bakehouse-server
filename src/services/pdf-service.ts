import handleBars from "handlebars";
import fs from "fs";
import pdf from "html-pdf";
import path from "path";
import configs from "../config/config";
import environment from "../environment";
import { formatDate } from "./date-service";
const config = (configs as { [key: string]: any })[environment];

type pdfargs = {
  orientation?: String;
  template: any;
  data?: any;
  fileName: any;
  uploadRoute?: any;
  border?: any;
  footerheight?: any;
  currentDate?: any;
};

// export const printPDF = async (pdfArgs: pdfargs) => {
//   return new Promise(async (resolve, reject) => {
//     let options: any = {
//       format: "A4",
//       orientation: "portrait",
//       border: {
//         top: "25px", // default is 0, units: mm, cm, in, px
//         right: "25px",
//         bottom: "25px",
//         left: "25px",
//       },
//       paginationOffset: 1, // Override the initial pagination number
//       header: {
//         height: "0mm",
//         contents: "",
//       },
//       footer: {
//         height: "10mm",
//         contents: {
//           // first: 'Cover page',
//           // 2: 'Second page', // Any page number is working. 1-based index
//           // default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
//           default:
//             '<div class="gradient"></div><div class="paging"><span style="color: #444;">{{page}}</span>/<span>{{pages}}</span></div>', // fallback value
//           // last: 'Last Page'
//         },
//       },
//     };

//     await fs.readFile(
//       pdfArgs.template,
//       {
//         encoding: "utf-8",
//       },
//       async (err, html) => {
//         if (err) {
//           reject(err);
//         } else {
//           let compiledHTML = await handleBars.compile(html)({
//             data: pdfArgs.data,
//           });

//           pdf
//             .create(compiledHTML, options)
//             .toFile(`${pdfArgs.uploadRoute}/${pdfArgs.fileName}`, (err) => {
//               if (err) {
//                 reject(err);
//               }
//               resolve(pdfArgs.fileName);
//             });
//         }
//       }
//     );
//   });
// };



export const printPDF = async (pdfArgs: pdfargs) => {
  let currentDate = formatDate(new Date(), "d/m/y");

  return new Promise(async (resolve, reject) => {
    let assetPath = path.join(`${__dirname}${config.localUrlAccess}`);
    assetPath = assetPath.replace(new RegExp(/\\/g), "/");
    let options: any = {
      format: "A4",
      orientation: pdfArgs.orientation ? pdfArgs.orientation : "portrait",
      base: `file:///${assetPath}`,
      localUrlAccess: true,
      timeout: "100000",

      border: pdfArgs.border
        ? pdfArgs.border
        : {
            top: "25px", // default is 0, units: mm, cm, in, px
            right: "25px",
            bottom: "25px",
            left: "25px",
          },
      childProcessOptions: {
        env: {
          OPENSSL_CONF: "/dev/null",
        },
      },
      paginationOffset: 1, // Override the initial pagination number
      header: {
        height: "0mm",
        contents: "",
      },
      footer: {
        height: pdfArgs.footerheight ? pdfArgs.footerheight : "5mm",
        contents: {
          // first: 'Cover page',
          // 2: 'Second page', // Any page number is working. 1-based index
          // default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value

          //   <div class="gradient"></div><div class="paging"><span style="color: #444;">{{page}}</span>/<span>{{pages}}</span><span>
          default: ` <table style="color:blue;font-size:46px;">
  <tbody>
    <tr>
      <td style="color:black;font-size:8px; width='600px'"><span style="color: #444;">{{page}}</span>/<span>{{pages}}</span></td>
      <td style="color:black;font-size:8px; width='100px'; text-align: end;">${currentDate}</td>
    </tr>
  </tbody>
</table>`,
          // </span></div>`, // fallback value
          //   last: currentDate,
        },
        //   "date": currentDate
      },
    };

    await fs.readFile(
      pdfArgs.template,
      {
        encoding: "utf-8",
      },
      async (err, html) => {
        if (err) {
          reject(err);
        } else {
          let compiledHTML = await handleBars.compile(html)({
            data: pdfArgs.data,
          });
          pdf
            .create(compiledHTML, options)
            .toFile(`${pdfArgs.uploadRoute}/${pdfArgs.fileName}`, (err) => {
              if (err) {
                reject(err);
              }
              resolve(pdfArgs.fileName);
            });
        }
      }
    );
  });
};