interface FnYearInterface {
  pattern: "xx-xx";
}

const Month = {
  January: 1,
  February: 2,
  March: 3,
  April: 4,
  May: 5,
  June: 6,
  July: 7,
  August: 8,
  September: 9,
  October: 10,
  November: 11,
  December: 12,
};

export class FnYear {
  constructor({ pattern }: FnYearInterface) {}

  current(): string {
    const today = new Date();
    const month = today.getMonth() + 1;
    let year: any = today.getFullYear();
    let former = year.toString().substring(2);    

    const latter = month >= Month.April ? Number(former) + 1 : Number(former) - 1;    

    return `${former}-${latter}`;
  }
}
