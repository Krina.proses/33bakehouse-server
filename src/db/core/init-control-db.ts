import type { Sequelize } from "sequelize";

import { User } from "../../routes/user/user-model";
import { Role } from "../../routes/role/role-model";
import { Menu } from "../../routes/menu/menu-model";
import { ActivityLogs } from "../../routes/activitylogs/activitylogs-model";
import { City } from "../../routes/city/city-model";
import { State } from "../../routes/state/state-model";
import { Country } from "../../routes/country/country-model";
import { Category } from "../../routes/category/category-model";
import { Item } from "../../routes/item/item-model";
// import { Attribute } from "../../routes/attribute/attribute-model";
// import { AttributeDetail } from "../../routes/attribute/attributeDetail-model";
// import { ItemDetail } from "../../routes/item/itemDetail-model";
import { ItemImages } from "../../routes/item/itemImages-model";
import { ItemOrder } from "../../routes/item-order/itemOrder-model";
import { ItemOrderDetail } from "../../routes/item-order/itemOrderDetail-model";
import { Cart } from "../../routes/cart/cart-model";
import { UserAddress } from "../../routes/user/userAddress-model";
import { OrderStatusHistory } from "../../routes/item-order/orderStatusHistory-model";

export {
  Menu,
  Role,
  User,
  Country,
  State,
  City,
  Category,
  // Attribute,
  // AttributeDetail,
  Item,
  // ItemDetail,
  ItemImages,
  ItemOrder,
  ItemOrderDetail,
  Cart,
  UserAddress,
  OrderStatusHistory,
};

export function initControlDB(sequelize: Sequelize) {
  Role.initModel(sequelize);
  User.initModel(sequelize);
  Menu.initModel(sequelize);
  ActivityLogs.initModel(sequelize);
  City.initModel(sequelize);
  State.initModel(sequelize);
  Country.initModel(sequelize);
  Category.initModel(sequelize);
  // Attribute.initModel(sequelize);
  // AttributeDetail.initModel(sequelize);
  Item.initModel(sequelize);
  // ItemDetail.initModel(sequelize);
  ItemImages.initModel(sequelize);
  ItemOrder.initModel(sequelize);
  ItemOrderDetail.initModel(sequelize);
  Cart.initModel(sequelize);
  UserAddress.initModel(sequelize);
  OrderStatusHistory.initModel(sequelize);

  User.belongsTo(Role, {
    as: "role",
    foreignKey: "roleId",
  });

  Role.hasMany(User, {
    as: "users",
    foreignKey: "roleId",
  });

  Country.hasMany(State, {
    as: "states",
    foreignKey: "countryId",
  });
  State.belongsTo(Country, {
    as: "country",
    foreignKey: "countryId",
  });
  State.hasMany(City, {
    as: "cities",
    foreignKey: "stateId",
  });

  City.belongsTo(State, {
    as: "state",
    foreignKey: "stateId",
  });

  // // Activity logs associations
  User.hasMany(ActivityLogs, {
    foreignKey: "userId",
  });
  ActivityLogs.belongsTo(User, {
    foreignKey: "userId",
  });

  // // Item and Category associations
  // Item.belongsTo(Category, {
  //   as: "itemCategory",
  //   foreignKey: "categoryId",
  // });

  // Category.hasMany(Item, {
  //   as: "categoryItem",
  //   foreignKey: "categoryId",
  // });

  // Attribute and AttributeDetail associations
  // AttributeDetail.belongsTo(Attribute, {
  //   foreignKey: "attributeId",
  // });

  // Attribute.hasMany(AttributeDetail, {
  //   foreignKey: "attributeId",
  // });

  // Item and Category associations
  Item.belongsTo(Category, {
    foreignKey: "categoryId",
  });

  Category.hasMany(Item, {
    foreignKey: "categoryId",
  });

  // // ItemDetail and Item associations
  // ItemDetail.belongsTo(Item, {
  //   foreignKey: "itemId",
  // });

  // Item.hasMany(ItemDetail, {
  //   foreignKey: "itemId",
  // });

  // // ItemDetail and Attribute associations
  // ItemDetail.belongsTo(Attribute, {
  //   foreignKey: "attributeId",
  // });

  // Attribute.hasMany(ItemDetail, {
  //   foreignKey: "attributeId",
  // });

  // // ItemDetail and AttributeDetail associations
  // ItemDetail.belongsTo(AttributeDetail, {
  //   foreignKey: "attributeDetailId",
  // });

  // AttributeDetail.hasMany(ItemDetail, {
  //   foreignKey: "attributeDetailId",
  // });

  // ItemImages and Item associations
  ItemImages.belongsTo(Item, {
    as: "itemImages",
    foreignKey: "itemId",
  });

  Item.hasMany(ItemImages, {
    as: "itemImages",
    foreignKey: "itemId",
  });

  // Cart and Item associations
  Cart.belongsTo(Item, {
    foreignKey: "itemId",
  });

  Item.hasMany(Cart, {
    foreignKey: "itemId",
  });

  // Cart and User associations
  Cart.belongsTo(User, {
    foreignKey: "userId",
  });

  User.hasMany(Cart, {
    foreignKey: "userId",
  });

  // UserAddress and User associations
  UserAddress.belongsTo(User, {
    foreignKey: "userId",
  });

  User.hasMany(UserAddress, {
    foreignKey: "userId",
  });

  // ItemOrder and User associations
  ItemOrder.belongsTo(User, {
    foreignKey: "userId",
  });

  User.hasMany(ItemOrder, {
    foreignKey: "userId",
  });

  // ItemOrderDetail and ItemOrder associations
  ItemOrderDetail.belongsTo(ItemOrder, {
    foreignKey: "orderId",
  });

  ItemOrder.hasMany(ItemOrderDetail, {
    foreignKey: "orderId",
  });

  // ItemOrderDetail and Item associations
  ItemOrderDetail.belongsTo(Item, {
    foreignKey: "itemId",
  });

  Item.hasMany(ItemOrderDetail, {
    foreignKey: "itemId",
  });

  // OrderStatusHistory and ItemOrder associations
  OrderStatusHistory.belongsTo(ItemOrder, {
    foreignKey: "orderId",
  });

  ItemOrder.hasMany(OrderStatusHistory, {
    foreignKey: "orderId",
  });

  // OrderStatusHistory and User associations
  OrderStatusHistory.belongsTo(User, {
    foreignKey: "userId",
  });

  User.hasMany(OrderStatusHistory, {
    foreignKey: "userId",
  });

  // ItemOrder and UserAddress associations
  ItemOrder.belongsTo(UserAddress, {
    foreignKey: "userAddressId",
  });

  UserAddress.hasMany(ItemOrder, {
    foreignKey: "userAddressId",
  });

  return {
    Role,
    City,
    User,
    Menu,
    ActivityLogs,
    State,
    Country,
    Category,
    // Attribute,
    // AttributeDetail,
    Item,
    // ItemDetail,
    ItemImages,
    ItemOrder,
    ItemOrderDetail,
    Cart,
    UserAddress,
    OrderStatusHistory,
  };
}
