import { Sequelize } from "sequelize/types";

const users = [
  {
    name:'Admin',
    mobile:9854565455,
    email: "admin@gmail.com",
    address: 'test',
    pincode:654565,
    password: "$2a$10$PAPE5WbuLTxiuO6rDV6.8et1LAAki8IowrqsZ.7lpfnzAs.sVvp26",
    roleId: 1,
    isActive: 1,
  },
];

export const seedUser = async (sequelize: Sequelize) => {
  return sequelize.getQueryInterface().bulkInsert("User", users);
};
