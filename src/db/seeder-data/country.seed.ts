import { Sequelize } from "sequelize/types";
import fs from "fs";
let rawdata: any = fs.readFileSync("src/routes/country/country.json");
const country = JSON.parse(rawdata);




export const seedCountry = async (sequelize: Sequelize, countryId: any) => {

  if(countryId) {
    let countryFilter = country.filter((item: any) => item.id == countryId)
    return sequelize.getQueryInterface().bulkInsert("Countries", countryFilter);
  }
  return sequelize.getQueryInterface().bulkInsert("Countries", country);
 
};