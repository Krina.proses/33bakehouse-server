import { Sequelize } from "sequelize/types";
import fs from "fs";
let rawdata: any = fs.readFileSync("src/routes/city/city.json");
let stateData: any = fs.readFileSync("src/routes/state/states.json");

const city = JSON.parse(rawdata);
const state = JSON.parse(stateData);




export const seedCity = async (sequelize: Sequelize, countryId: any) => {
  if (countryId) {
    let stateFilter = state.filter((item: any) => item.countryId == countryId)
    for (const item of stateFilter) {
      let getStateWiseCity = city.filter((i: any) => {
        return i.stateId == item.id
      })
      if (getStateWiseCity && getStateWiseCity.length > 0) {
        await sequelize.getQueryInterface().bulkInsert("Cities", getStateWiseCity);

      }

    }
  }else{
    let myArr: any[] = [...city]
    let curr = 0, next = 10000, result: any[] = [];
  
    for (let i = curr; i < next; i++) {
      result.push(myArr[i])
      if (i == next - 1) {
        next += 10001
        await sequelize.getQueryInterface().bulkInsert("City", result);
        result.length = 0;
      }
      if (i > 150013 && i < 150710) {
        await sequelize.getQueryInterface().bulkInsert("City", result);
        result.length = 0;
      }
      if (i > 150709) {
        break;
      }
    }
  }
}