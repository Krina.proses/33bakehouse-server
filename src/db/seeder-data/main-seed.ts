import {Sequelize  } from "sequelize";

import { seedUser } from "./user-seed";
import { seedRoles } from "./role-seed";
import { seedMenu } from "./menu.seed";
import { seedCountry } from "./country.seed";
import { seedState } from "./state.seed";
import { seedCity } from "./city.seed";


export const runSeeders = async (sequelize: Sequelize) => {
    console.log("running seeders")
    await seedRoles(sequelize);
    await seedUser(sequelize);
    await seedMenu(sequelize);
    
    await seedCountry(sequelize, 101); // if country wise data dump pass contryid other wise pass null
    await seedState(sequelize, 101);  // if country wise data dump pass contryid other wise pass null
    await seedCity(sequelize, 101);  // if country wise data dump pass contryid other wise pass null


}