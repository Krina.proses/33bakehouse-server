import { Sequelize } from "sequelize/types";


const menu = [
  {id: 1, link: "/", parentID: null, title: "Dashboard", icon: "fa fa-th-large", isActive:1},
  {id: 2, link: "/user-management", parentID: null, title: "User Management", icon:"fas fa-user-friends", isActive:1},
  {id: 3, link: "/user-management/user/list", parentID: 2, title: "User", icon: "fas fa-caret-right fa-fw" , isActive:1},
  {id: 4, link: "/user-management/permission", parentID: 2, title: "Permission", icon: "fas fa-caret-right fa-fw" , isActive:1},
  {id: 5, link: "/user-management/roles/list", parentID: 2, title: "Roles", icon: "fas fa-caret-right fa-fw" , isActive:1},
  {id: 6, link: "", parentID: null, title: "Location", icon: "fas fa-location-dot" , isActive:1},
  {id: 7, link: "/location/country/list", parentID: 6, title: "Country", icon: "fas fa-caret-right fa-fw"  , isActive:1},
  {id: 8, link: "/location/state/list", parentID: 6, title: "State", icon: "fas fa-caret-right fa-fw"  , isActive:1},
  {id: 9, link: "/location/city/list", parentID: 6, title: "City", icon: "fas fa-caret-right fa-fw"  , isActive:1},
  {id: 10, link: "/activity-logger/list", parentID: null, title: "Activity Log", icon: "fas fa-caret-right fa-fw"  , isActive:1},
  {id: 11, link: null, parentID: null, title: "Master", icon: "fa fa-list", isActive: 1},
  {id: 12, link: "/master/category", parentID: 11, title: "Category", icon: "fa fa-circle", isActive: 1},
  // {id: 13, link: "/master/attribute", parentID: 11, title: "Attribute", icon: "fa fa-circle", isActive: 1},
  {id: 13, link: "/master/item", parentID: 11, title: "Item Master", icon: "fa fa-circle", isActive: 1},
  {id: 14, link: "/order-detail", parentID: null, title: "Order Detail", icon: "fa fa-circle", isActive: 1},

  

];

export const seedMenu = async (sequelize: Sequelize) => {  
  return sequelize.getQueryInterface().bulkInsert("Menu", menu);
};
