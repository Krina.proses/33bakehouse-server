import { Sequelize } from "sequelize/types";
import fs from "fs";
let rawdata: any = fs.readFileSync("src/routes/state/states.json");

const state = JSON.parse(rawdata);
let cityrawdata: any = fs.readFileSync("src/routes/city/city.json");
const city = JSON.parse(cityrawdata);



let statesList: any[] = []
export const seedState = async (sequelize: Sequelize, countryId: any) => {

  if (countryId) {
    let stateFilter = state.filter((item: any) => item.countryId == countryId)
    statesList = stateFilter
    return sequelize.getQueryInterface().bulkInsert("States", stateFilter);
  }
  return sequelize.getQueryInterface().bulkInsert("States", state);
};


export const seedStateCities = async (sequelize: Sequelize) => {
  let cityFilter: any
  let cityData: any[] = []
  statesList.forEach((data: any) => {
    cityFilter = city.filter((item: any) => item.stateId == data.id);
    cityData.push(...cityFilter);
  });
  return sequelize.getQueryInterface().bulkInsert("City", cityData);
}