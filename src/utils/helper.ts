import { NumService } from "proses-numgen";
import { GLOBAL_CONSTANTS } from "./constants";
import { compareAsc, format } from "date-fns";
import configs from "../config/config";
import environment from "../environment";
import {
  createCusCart,
  findData,
  updateQuantity,
} from "../routes/cart/cart-handler";
import { FnYear } from "../services/year-service";
import { ItemOrder } from "../routes/item-order/itemOrder-model";
import { Op } from "sequelize";
const config = (configs as { [key: string]: any })[environment];

export const sanitizeBody = (obj: any) => {
  try {
    let sanitizedObject: any = {};

    for (let key of Object.keys(obj)) {
      if (obj[key] == false || obj[key]) {
        if (obj[key] == "" || obj[key] == "null") {
          sanitizedObject[key] = null;
          continue;
        } else {
          sanitizedObject[key] = obj[key];
        }
      }
    }

    if (!Object.keys(sanitizedObject).length) {
      return false;
    }
    return sanitizedObject;
  } catch (error) {
    return false;
  }
};

export const getAdminLink = (url: string) => {
  return `${config.adminURL}/#/${url}`;
};
export const getServerLink = (url: string) => {
  return `${config.ApiUrl}/${url}`;
};

export const getReactLink = (url: string) => {
  return `${config.reactUrl}/${url}`;
};

export const getTime = (time: any, hours: any, minit?: any) => {
  const strTime = time.replace(/ /g, "");

  let startTime: any;
  if (strTime.includes("to")) {
    startTime = strTime.split("to")[0];
  } else {
    startTime = strTime;
  }
  const index = startTime.search(/[a-zA-Z]/);
  const result = [startTime.slice(0, index), startTime.slice(index).trim()];
  let str: any;
  if (result[0].length == 1) {
    str = `0${result[0]}:00`;
  } else {
    str = `${result[0]}:00`;
  }
  let newTime: any = str
    .toString()
    .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [str];
  if (newTime.length > 1) {
    // If time format correct
    newTime = newTime.slice(1); // Remove full string match value

    // newTime[0] = +newTime[0] % 12 || 12; // Adjust hours
  }

  let newHours = Number(newTime[0]) + hours;
  let newMinit: any = "00";
  if (minit) {
    newMinit = Number(newTime[2]) + minit;
  }
  if (["pm", "PM", "Pm"].includes(result[1])) {
    result[1] = result[1].replace(/pm|Pm|PM/, "PM");

    if (newHours > 12) {
      result[1] = result[1].replace(/pm|Pm|PM/, "AM");
      newHours = +newHours % 12 || 12; // Adjust hours
    }
  } else {
    result[1] = result[1].replace(/am|AM|Am/, "AM");

    if (newHours > 12) {
      result[1] = result[1].replace(/am|AM|Am/, "PM");
      newHours = +newHours % 12 || 12;
    }
  }
  return `${newHours}.${newMinit} ${result[1]}`;
};

export const dateFormate = (date: any) => {
  const [day, month, year]: any = date.split("/");
  const newDate = new Date(+year, month - 1, +day);
  const formateDate = format(new Date(newDate), "MM-dd-yyyy");
  return formateDate;
};

export const formatDate = (date: any, format: string = "") => {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  if (format == "m-d-y") {
    return [month, year, day].join("-");
  } else if (format == "d-m-y") {
    return [day, month, year].join("-");
  } else if (format == "d-m") {
    return [day, month].join("-");
  } else if (format == "d/m/y") {
    return [day, month, year].join("/");
  } else if (format == "y-d-m") {
    return [year, day, month].join("-");
  } else {
    return [year, month, day].join("-");
  }
};

export const addToCartFunc: (cartItems: any, userId: any, t: any) => void = async (
  cartItems: any,
  userId: any,
  t: any
) => {
  let { itemId, amount } = cartItems;
  
  const cartdata: any = await findData({ itemId }, userId);

  let addcart: any;

  if (cartdata) {
    let ID = Number(cartdata.id);

    let updatedData = {
      quantity: cartdata.quantity + Number(cartItems?.quantity),
      amount: cartdata.amount + Number(cartItems?.amount),
      modifiedBy: userId,
    };

    addcart = await updateQuantity(updatedData, ID, t);
  } else {
    let insertCart = {};

    insertCart = {
      ...cartItems,
      isActive: 1,
      //@ts-ignore
      userId: userId,
      createdBy: userId,
    };
    addcart = await createCusCart(insertCart, t);
  }

  return addcart
};

export const generateInvoiceNo = async (body: any) => {
  const prefix = "INV";
  const currentFnYear = new FnYear({ pattern: "xx-xx" }).current();
  let fnYear = `${prefix}${currentFnYear}`;

  const payment = await ItemOrder.lastInvoiceByFees(fnYear);
  
  let invoiceNo = payment?.invoiceNo || `${prefix}${currentFnYear}/000000`;  

  // let idsToBeUpdated: { id: number }[] = [];

//  let idsToBeUpdated: any = await ItemOrder.includedPayments(body);
//   console.log(idsToBeUpdated,"idsToBeUpdated");
  

  // //core logic to generate invoice numbers
  // const length = idsToBeUpdated.length;
  // console.log(length, "length");

  const nums = new NumService({
    value: invoiceNo,
    seperator: "/",
  }).increment();

  return nums.value;
};
