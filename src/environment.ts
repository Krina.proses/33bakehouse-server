let env = {
  development: "development",
  production: "production",
  staging: "staging",
};

export default env.development;
