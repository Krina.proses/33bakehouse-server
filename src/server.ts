import app from "./app";
import controlDB from "./db/core/control-db";
import { initControlDB } from "./db/core/init-control-db";

async function run() {
  initControlDB(controlDB);

  // const port = 9001
  const unsecurePort = 9002;
  app.listen(unsecurePort, () => {
    console.log(`http server running at ${unsecurePort}`);
  });
}

run();
