import express, { Application, Request } from "express";
import cors from "cors";
import helmet from "helmet";
import compression from "compression";
import routes from "./routes/route-index";
import errorHandler from "./db/core/logger/error-logger";
import currentUser from "./services/current-user";

//not used
// import { initProsesConfig } from "./config/init-proses";
// import loadService from "./services/load-service";
// import errorHandler from "./middlewares/errorHandler.middleware"
const app: Application = express();
const fs = require("fs");

app
  .use(cors())
  .use("/static", express.static("src/public"))
  .use(helmet())
  .use(compression())
  .use(express.json())
  .use(express.urlencoded({ limit: "50mb", extended: true }))
  .use(
    helmet.frameguard({
      action: "deny",
    })
  );



//global error handler
// app.use(errorHandler);


app.post('*', (req, res, next) => {
  req.query = hydrateUser(req.query)
  next();
});

app.put('*', (req, res, next) => {
  req.query = hydrateUser(req.query)
  next();
});


(routes as any)(app);

// app.use("/", (req, res) => {
//   res.status(404).send("Route Not Found");
// });
export default app;


function hydrateUser(query: any) {
  if (!query.userData) {
    return query;
  }

  currentUser.hydrate(JSON.parse(query.userData));
  delete query.userData;
  return query
}