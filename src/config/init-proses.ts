import loggerInstance from "proses-logger";
import { responseHandler } from "proses-response";
import prosesjwt from "proses-jwt";

export const initProsesConfig = () => {
  loggerInstance.init({ path: "./logs/info.log" });
  responseHandler.registerLoggers({
    err: (err: any) => {
      console.log(err);
    },
  });
  responseHandler.registerDialect("mysql");
  prosesjwt.init({
    secret: "",
    expiry: "",
  });
};
