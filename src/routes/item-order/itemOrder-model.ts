import { CreationOptional, DataTypes, Model, Sequelize, Op } from "sequelize";

export class ItemOrder extends Model {
  declare id: CreationOptional<number>;
  declare userId: number;
  declare userAddressId: number;
  declare orderNo: number;
  declare billingAddress: string;
  declare shippingAddress: string;
  declare billingAsShipping: boolean;
  declare paymentType: string;
  declare orderStatus: string;
  declare trackingCode: string;
  declare trackingLink: string;
  declare subTotal: number;
  declare total: number;
  declare deliveryCharges: number;
  declare isRegister: boolean;
  declare cancelledByUser: number;
  declare isInvoice: boolean;
  declare invoiceNo: string;
  declare invoiceDate: string;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
  //invoice created on stored in milliseconds
  declare invoiceCreatedOn: CreationOptional<number | null>;

  //declare association here
  declare static associations: {};

  static includedPayments(included: number[]) {
    console.log(included,"included");
    
    return ItemOrder.findOne({
      where: {
        [Op.and]: [
          { id: included },
          { isInvoice: { [Op.or]: [false, null, 0] } },
        ],
      },
      attributes: ["id"],
      raw: true,
    });
  }

  static lastInvoiceByFees(fnYear: any) {
    return ItemOrder.findOne({
      where: { invoiceNo : { [Op.like]: `%${fnYear}%`} },
      limit: 1,
      attributes: ["invoiceNo"],
      raw: true,
      order: [["invoiceCreatedOn", "DESC"]],
    });
  }

  static initModel(sequelize: Sequelize): typeof ItemOrder {
    ItemOrder.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        userId: {
          type: DataTypes.INTEGER,
        },
        userAddressId: {
          type: DataTypes.INTEGER,
        },
        orderNo: {
          type: DataTypes.INTEGER,
        },
        billingAddress: {
          type: DataTypes.TEXT,
        },
        shippingAddress: {
          type: DataTypes.TEXT,
        },
        billingAsShipping: {
          type: DataTypes.BOOLEAN,
        },
        paymentType: {
          type: DataTypes.STRING(255),
          // type: DataTypes.ENUM({
          //   values: [
          //     "CashOnDelivery",
          //     "OnlinePayment",
          //     "Offline-Bank"
          //   ],
          // }),
        },
        orderStatus: {
          type: DataTypes.ENUM({
            values: [
              "Pending",
              "AwaitingPayment",
              "DeclinedPayment",
              "OrderPlaced",
              "Processing",
              "Dispatched",
              "Delivered",
              "Cancelled",
              "Refunded",
            ],
          }),
          defaultValue: "Pending",
        },
        trackingCode: { type: DataTypes.STRING },
        trackingLink: { type: DataTypes.STRING },
        subTotal: { type: DataTypes.FLOAT },
        total: { type: DataTypes.FLOAT },
        deliveryCharges: { type: DataTypes.FLOAT },
        isRegister: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0,
        },
        cancelledByUser: { type: DataTypes.INTEGER },
        isInvoice: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0,
        },
        invoiceNo: { type: DataTypes.STRING },
        invoiceDate: { type: DataTypes.DATE },
        invoiceCreatedOn: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return ItemOrder;
  }
}
