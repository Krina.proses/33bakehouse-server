import { CreationOptional, DataTypes, Model, Sequelize } from "sequelize";

export class ItemOrderDetail extends Model {
  declare id: CreationOptional<number>;
  declare orderId: number;
  declare itemId: number;
  declare quantity: number;
  declare amount: number;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {};

  static initModel(sequelize: Sequelize): typeof ItemOrderDetail {
    ItemOrderDetail.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        orderId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        itemId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        quantity: {
          type: DataTypes.INTEGER,
        },
        amount: {
          type: DataTypes.INTEGER,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return ItemOrderDetail;
  }
}
