import {
  Item,
  Category,
  // ItemDetail,
  // Attribute,
  // AttributeDetail,
  ItemImages,
  ItemOrder,
  ItemOrderDetail,
  User,
  OrderStatusHistory,
  UserAddress,
} from "../../db/core/init-control-db";
import { Op, Transaction, where } from "sequelize";
const { MakeQuery } = require("../../services/model-service");

//for get order filter
export const getAllOrderFilterWise = async (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } =
    MakeQuery({
      query: query,
      Model: ItemOrder,
    });

  let includeOption = [
    {
      model: User,
      attributes: ["name", "email", "mobile", "address", "pincode", "roleId"],
    },
  ];

  if (forExcel) {
    return ItemOrder.findAll({
      where: modelOption,
      attributes,
      // include: includeOption,
      order: orderBy,
      // raw: true,
    });
  } else {
    return ItemOrder.findAndCountAll({
      where: modelOption,
      attributes,
      include: includeOption,
      order: orderBy,
      limit,
      subQuery: false,
      distinct: true,
      // raw: true,
      offset,
    });
  }
};

//for find for count
export const findForCount = (order: any, limit: number) => {
  return ItemOrder.findOne({ order, limit, raw: true });
};

//place order
export const createOrder = (body: any, t: any) => {
  return ItemOrder.create(body, { transaction: t });
};

//for order details
export const orderDetails = async (body: any, t: any) => {
  return ItemOrderDetail.bulkCreate(body, { transaction: t });
};


//get single order with login
export const getItemOrderById = (params: any) => {
  return ItemOrder.findOne({
    where: { id: params.id },
    include: [
      {
        model: ItemOrderDetail,
        attributes: ["id", "orderId", "itemId", "quantity", "amount"],
        include: [
          {
            model: Item,
            attributes: ["id", "categoryId", "itemName", "basicPrice"],
          },
        ],
      },
      {
        model: User,
        attributes: ["name", "email", "mobile", "address", "pincode", "roleId"],
      },
    ],
  });
};

export const addstatusHistory = (body: any, t: any) => {
  return OrderStatusHistory.create(body, { transaction: t });
};

export const updateOrder = (body: any, params: any, t: any) => {
  return ItemOrder.update(body, {
    where: {
      id: params.id,
    },
    transaction: t
  });
};

export const getAllOrderList = () => {
  return ItemOrder.findAll({
    where: {
      isRegister: 1
    },
    include: [
      {
        model: ItemOrderDetail,
        attributes: ["id", "orderId", "itemId", "quantity", "amount"],
        include: [
          {
            model: Item,
            attributes: ["id", "categoryId", "itemName", "basicPrice"],
            include: [
              {
                model: ItemImages,
                as: "itemImages",
                attributes: ["id", "itemId", "image"],
              },
            ],
          },
        ],
      },
      {
        model: User,
        attributes: ["name", "email", "mobile", "address", "pincode", "roleId"],
      },
      {
        model: OrderStatusHistory,
        attributes: ["id", "userId", "orderId", "orderStatus", "createdAt"],
      },
    ],
    order: [["orderNo", "DESC"]],
  });
};

export const getOrderTrackByEmail = (email: any) => {
  return UserAddress.findOne({
    where: {
      email,
    },
    include: [
      {
        model: ItemOrder,
        include: [
          {
            model: ItemOrderDetail,
            include: [
              {
                model: Item,
                attributes: ["id", "categoryId", "itemName", "basicPrice"],
                include: [
                  {
                    model: ItemImages,
                    as: "itemImages",
                    attributes: ["id", "itemId", "image"],
                  },
                ],
              },
            ],
          },
          {
            model: OrderStatusHistory,
            attributes: ["id", "userId", "orderId", "orderStatus", "createdAt"],
          },
        ],
      },
    ],
  });
};

export const getOrderTrackByOrderNo = (orderNo: any) => {
  return ItemOrder.findOne({
    where: {
      orderNo,
    },
    include: [
      {
        model: ItemOrderDetail,
        attributes: ["id", "orderId", "itemId", "quantity", "amount"],
        include: [
          {
            model: Item,
            attributes: ["id", "categoryId", "itemName", "basicPrice", "description"],
            include: [
              {
                model: ItemImages,
                as: "itemImages",
                attributes: ["id", "itemId", "image"],
              },
            ],
          },
        ],
      },
      {
        model: OrderStatusHistory,
        attributes: ["id", "userId", "orderId", "orderStatus", "createdAt"],
      },
      {
        model: UserAddress,
      },
    ],
  });
};


// without login placeorder
export const singleOrder = (params: any) => {
  return ItemOrder.findOne({
    where: { id: params.id },
    include: [
      {
        model: ItemOrderDetail,
        attributes: ["id", "orderId", "itemId", "quantity", "amount"],
        include: [
          {
            model: Item,
            attributes: ["id", "categoryId", "itemName", "basicPrice",  "description"],
            include: [
              {
                model: ItemImages,
                as: "itemImages",
                attributes: ["id", "itemId", "image"],
              },
            ],
          },
        ],
      },
      {
        model: OrderStatusHistory,
        attributes: ["id", "userId", "orderId", "orderStatus", "createdAt"],
      },
      {
        model: User,
        attributes: ["name", "email", "mobile", "address", "pincode", "roleId"],
      },
      {
        model: UserAddress,
      },
    ],
  });
};

export const SingleOrderByOrderNo = (orderNo: number) => {
  return ItemOrder.findOne({
    where: {
      orderNo
    }, 
    include: [
      {
        model: ItemOrderDetail,
        attributes: ["id", "orderId", "itemId", "quantity", "amount"],
        include: [
          {
            model: Item,
            attributes: ["id", "categoryId", "itemName", "basicPrice",  "description"],
            include: [
              {
                model: ItemImages,
                as: "itemImages",
                attributes: ["id", "itemId", "image"],
              },
            ],
          },
        ],
      },
      {
        model: OrderStatusHistory,
        attributes: ["id", "userId", "orderId", "orderStatus", "createdAt"],
      },
      {
        model: UserAddress,
      },
    ],
  })
}
