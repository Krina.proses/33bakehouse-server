import { CreationOptional, DataTypes, Model, Sequelize } from "sequelize";

export class OrderStatusHistory extends Model {
  declare id: CreationOptional<number>;
  declare userId: number;
  declare userAddressId: number;
  declare orderId: number;
  declare orderStatus: string;
  declare isRegister: boolean;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {};

  static initModel(sequelize: Sequelize): typeof OrderStatusHistory {
    OrderStatusHistory.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        userId: {
            type: DataTypes.INTEGER,
        },
        orderId: {
          type: DataTypes.INTEGER,
        },
        orderStatus: {
          type: DataTypes.ENUM({
            values: [
              "Pending",
              "AwaitingPayment",
              "DeclinedPayment",
              "OrderPlaced",
              "Processing",
              "Dispatched",
              "Delivered",
              "Cancelled",
              "Refunded",
            ],
          }),
          allowNull: false,
        },
        isRegister: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return OrderStatusHistory;
  }
}
