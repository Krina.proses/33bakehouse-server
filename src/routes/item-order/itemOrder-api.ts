import express from "express";
import { unlink } from "fs";
import prosesjwt from "proses-jwt";
import { serverError, success } from "proses-response";
import configs from "../../config/config";
import ErrorLogger from "../../db/core/logger/error-logger";
import environment from "../../environment";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { sendEmail } from "../../services/mailService";
import { printPDF } from "../../services/pdf-service";
import { GLOBAL_CONSTANTS } from "../../utils/constants";
import { formatDate, generateInvoiceNo } from "../../utils/helper";
import { deleteAllCartItem } from "../cart/cart-handler";
import { createaddressData } from "../user/user-handler";
import {
  SingleOrderByOrderNo,
  addstatusHistory,
  createOrder,
  findForCount,
  getAllOrderFilterWise,
  getAllOrderList,
  getItemOrderById,
  getOrderTrackByEmail,
  getOrderTrackByOrderNo,
  orderDetails,
  singleOrder,
  updateOrder,
} from "./itemOrder-handler";
import dbInstance from "../../db/core/control-db";
const config = (configs as { [key: string]: any })[environment];

const cheerio = require("cheerio");

let { tokenMiddleWare } = prosesjwt;

const router = express.Router();

//get All item (paginated)
router.get("/getAllOrderFilterWise", tokenMiddleWare, async (req, res) => {
  try {
    let orderData = await getAllOrderFilterWise(req.query);
    sendEncryptedResponse(res, orderData, "get All Order");
  } catch (error) {
    ErrorLogger.write({ type: "getAllOrder error", error });
    serverError(res, error);
  }
});

router.get("/getItemOrderById/:id", tokenMiddleWare, async (req, res) => {
  try {
    let orderData = await getItemOrderById(req.params);
    sendEncryptedResponse(res, orderData, "get All Order");
  } catch (error) {
    console.log(error, "errorrrrrrrrrr");
    serverError(res, error);
  }
});

// place order
router.post("/placeOrder-user", tokenMiddleWare, async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {
    let userId = req.user.id;
    // customer email
    let userEmail = req.user.email;

    //place orders
    const order = [["orderNo", "DESC"]];
    const limit = 1;

    let Table: any = await findForCount(order, limit);

    let num: any;
    if (Table == null) {
      const str2 = "1";
      num = str2;
    } else {
      if (Table.orderNo) {
        let seqNumber = parseInt(Table.orderNo);

        seqNumber++;
        // let newseqNumber = JSON.stringify(seqNumber);
        num = seqNumber;
      }
    }

    let placeOrder = {
      orderNo: num,
      billingAddress: req.body.billingAddress,
      userId: userId,
      shippingAddress: req.body.shippingAddress,
      billingAsShipping: req.body.billingAsShipping,
      paymentType: req.body.paymentType,
      orderStatus: req.body.orderStatus,
      subTotal: req.body.subTotal,
      deliveryCharges: req.body.deliveryCharges,
      total: req.body.total,
      isRegister: 1,
      isInvoice: req.body.isInvoice,
    };


    let orders = await createOrder(placeOrder, t);
    //end

    let invoiceNo = await generateInvoiceNo(orders.id);

    let invoiceObj = {
      invoiceNo: invoiceNo,
      invoiceDate: new Date()
    }

    let params: any = { id: orders.id }

    await updateOrder(invoiceObj, params, t)

    
    //start
    let orderDetail: any = [];
    let Details = [];

    if (req.body.cartList) {
      let cartIndex = 0;
      while (cartIndex < req.body.cartList.length) {
        Details.push({
          orderId: orders.id,
          itemId: req.body.cartList[cartIndex].id,
          quantity: req.body.cartList[cartIndex].QTY
            ? req.body.cartList[cartIndex].QTY
            : req.body.cartList[cartIndex].quantity,
          amount: req.body.cartList[cartIndex].amount,
          createdBy: userId,
        });

        cartIndex++;
      }

      orderDetail = await orderDetails(Details, t);
    }

    await deleteAllCartItem({ id: userId });

    let statusHistory: any = {
      userId: userId,
      orderId: orders.id,
      orderStatus: orders.orderStatus,
      isRegister: 1,
    };

    await addstatusHistory(statusHistory, t);

    // let params = {
    //   id: orders.id
    // }

    let singleOrder: any = await getItemOrderById(params);
    singleOrder = await singleOrder?.get({ plain: true });
  
    //send email for place order
    const mailData: any = {
      to: userEmail,
      subject: "33BakeHouse-New Order Place",
      html: `<div>
              <h3>Hi, ${req.user?.name || "Customer"}</h3>
              <p>
                <strong>Your order has been placed successfully.</strong><br /><br />
                Your order number is <strong>${
                  singleOrder?.orderNo || "-"
                }</strong> <br />
                and status is <strong>${
                  singleOrder?.orderStatus || "-"
                }</strong>.<br /><br /><br /><br /><br />
                please download invoice from the attachments
              </p>
            </div>`,
      // attachments: [
      //   {
      //     filename: fileName,
      //     path: `./public/pdf/${fileName}`,
      //   },
      // ],
    };

    sendEmail(mailData);

    await t.commit();
    success(res, { orders, orderDetail }, "placed order successfully");

  } catch (error) {
    console.log(error,"errorrrrrr");
    
    await t.rollback();
    serverError(res, error);
  }
});

router.put("/updateorderStatus/:id", async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {

    let order: any = await getItemOrderById(req.params);

    let statusHistory: any = {
      userId: req.body.userId,
      orderId: order.id,
      orderStatus: req.body.orderStatus,
      isRegister: 1,
    };

    await addstatusHistory(statusHistory, t);

    let updateStatus: any = {
      orderStatus: req.body.orderStatus,
    };
    let orders = await updateOrder(updateStatus, req.params, t);

    // send email - status change
    const mailData: any = {
      to: order?.User?.email,
      subject: "33BakeHouse Order Status",
      htmlFile: "orderStatus",
      // html: `<p> Your current order status is <strong>${order.orderStatus}</strong></p>.`,
      replacements: {
        orderInfo: {
          userName: order?.User?.name,
          orderNo: order?.orderNo,
          orderStatus: req.body.orderStatus,
          // trackingCode:
          //   req.body.orderStatus === STATUS_LIST.Dispatched
          //     ? req.body.trackingCode
          //     : null,
          // trackingLink:
          //   req.body.orderStatus === STATUS_LIST.Dispatched
          //     ? req.body.trackingLink
          //     : null,
        },
      },
    };

    sendEmail(mailData);

    await t.commit();
    sendEncryptedResponse(res, orders, "Data is updated");
  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

router.get("/getAllOrderList", tokenMiddleWare, async (req, res) => {
  try {
    let orderData = await getAllOrderList();
    success(res, orderData, "get All Order");
  } catch (error) {
    ErrorLogger.write({ type: "getAllOrder error", error });
    serverError(res, error);
  }
});

// place order
router.post("/withoutLoginplaceOrder-user", async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {

    let userEmail = req.body.email;

    let userObj = {
      name: req.body.name,
      mobile: req.body.mobile,
      email: req.body.email,
      address: req.body.address,
      cityName: req.body.cityName,
      pincode: req.body.pincode,
      isLogin: 0,
      isActive: 1,
    };

    let userDetail: any = await createaddressData(userObj, t);

    //place orders
    const order = [["orderNo", "DESC"]];
    const limit = 1;

    let Table: any = await findForCount(order, limit);

    let num: any;
    if (Table == null) {
      const str2 = "1";
      num = str2;
    } else {
      if (Table.orderNo) {
        let seqNumber = parseInt(Table.orderNo);

        seqNumber++;
        num = seqNumber;
      }
    }

    let placeOrder = {
      orderNo: num,
      // userId: userDetail.id,
      userAddressId: userDetail.id,
      shippingAddress: req.body.shippingAddress,
      paymentType: req.body.paymentType,
      orderStatus: req.body.orderStatus,
      subTotal: req.body.subTotal,
      deliveryCharges: req.body.deliveryCharges,
      total: req.body.total,
      isRegister: 0,
      isInvoice: req.body.isInvoice,
    };

    let orders = await createOrder(placeOrder, t);
    //end

    
    let invoiceNo = await generateInvoiceNo(orders.id);

    let invoiceObj = {
      invoiceNo: invoiceNo,
      invoiceDate: new Date()
    }

    let params = { id: orders.id }

    await updateOrder(invoiceObj, params, t)

    //start
    let orderDetail: any = [];
    let Details = [];

    if (req.body.cartList) {
      let cartIndex = 0;
      while (cartIndex < req.body.cartList.length) {
        Details.push({
          orderId: orders.id,
          itemId: req.body.cartList[cartIndex].itemId,
          quantity: req.body.cartList[cartIndex].QTY
            ? req.body.cartList[cartIndex].QTY
            : req.body.cartList[cartIndex].quantity,
          amount: req.body.cartList[cartIndex].amount,
        });
        cartIndex++;
      }

      orderDetail = await orderDetails(Details, t);
    }

    let statusHistory: any = {
      userId: userDetail.id,
      orderId: orders.id,
      orderStatus: orders.orderStatus,
      isRegister: 0,
    };

    await addstatusHistory(statusHistory, t);

    let singleOrder: any = await getItemOrderById(orders);
    singleOrder = await singleOrder?.get({ plain: true });

    //send email for place order
    const mailData: any = {
      to: userEmail,
      subject: "33BakeHouse-New Order Place",
      html: `<div>
              <h3>Hi, ${userDetail?.name || "Customer"}</h3>
              <p>
                <strong>Your order has been placed successfully.</strong><br /><br />
                Your order number is <strong>${
                  singleOrder?.orderNo || "-"
                }</strong> <br />
                and status is <strong>${
                  singleOrder?.orderStatus || "-"
                }</strong>.<br /><br /><br /><br /><br />
                please download invoice from the attachments
              </p>
            </div>`,
      // attachments: [
      //   {
      //     filename: fileName,
      //     path: `./public/pdf/${fileName}`,
      //   },
      // ],
    };

    sendEmail(mailData);
    await t.commit();
    success(res, { orders, orderDetail }, "placed order successfully");

  } catch (error) {
    console.log(error, "errorrrrr");
    await t.rollback();
    serverError(res, error);
  }
});

//get All order (track order)
router.get("/getOrderTrack/:search", async (req, res) => {
  try {
    let orderData: any;

    if (GLOBAL_CONSTANTS.emailRegex.test(req.params.search)) {
      orderData = await getOrderTrackByEmail(req.params.search);
    } else {
      orderData = await getOrderTrackByOrderNo(req.params.search);
    }

    success(res, orderData, "get All Order");
  } catch (error) {
    console.log(error, "errorrrr");

    ErrorLogger.write({ type: "getAllOrder error", error });
    serverError(res, error);
  }
});

const insertSequenceNo = (itemOrderArray: any[]) => {
  let srNo = 0;
  return itemOrderArray.map((item) => {
    srNo++;
    return { ...item, srNo };
  });
};

//get order pdf (track order)
router.get("/productOrderPDF/:id", async (req, res) => {
  try {

    let orderData: any = await singleOrder(req.params);
    orderData = JSON.parse(JSON.stringify(orderData));

    let itemOrderArray: any[] = [];

    for (let item of orderData.ItemOrderDetails) {
      // Your HTML string
      let htmlString = item.Item.description;
      const htmlDta = cheerio.load(htmlString);
      const textContent = htmlDta("body").text();

      item.Item.description = textContent;
      itemOrderArray.push(item);
    }

    itemOrderArray = insertSequenceNo(itemOrderArray);

    let pdfPayload = {
      orderData,
      user: orderData.userId ? orderData.User : orderData.UserAddress,
      invoiceNo: orderData.invoiceNo,
      invoiceDate: formatDate(orderData.invoiceDate, 'd-m-y'),
      itemOrderArray,
    };

    const pdfargs = {
      template: `${config.templatePath}/productInvoice.html`,
      fileName: `ProductInvoice.pdf`,
      data: pdfPayload,
      uploadRoute: `${config.publicPath}/ProductInvoicepdf/`,
    };

    const pdf = await printPDF(pdfargs);
    success(res, pdf, "Get Members Pdf");

    let filePath = `${pdfargs.uploadRoute}/${pdfargs.fileName}`;

    setTimeout(() => {
      unlink(`${filePath}`, (err) => {});
    }, 1000);
  } catch (error) {
    serverError(res, error);
  }
});

//get all data
router.get("/SingleOrderByOrderNo/:OrderNo", async (req: any, res: any) => {
  try {

    
    let order = await SingleOrderByOrderNo(req.params.OrderNo);
    success(res, order, "Get Data successfully");
  } catch (error) {
    serverError(res, error);
  }
});

module.exports = router;
