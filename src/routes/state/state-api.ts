import express from "express";
import {
  addState,
  checkStateName,
  deleteState,
  getAllActiveState,
  getAllState,
  getStatebyID,
  updateState,
  dumpAllStates,
  getActiveState,
} from "./state-handler";
import { alreadyExist, serverError, success } from "proses-response";
import prosesjwt from "proses-jwt";
const router = express.Router();
import dbInstance from "../../db/core/control-db";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { State } from "../../db/core/init-control-db";
const { tokenMiddleWare } = prosesjwt;

//findAllState
router.get("/getAllState", async (req, res) => {
  try {
    let allState: any = await getAllState(req.query);
    sendEncryptedResponse(res,  allState, "Get all states");

  } catch (error) {
    serverError(res, error);
  }
});

//findAllState react side
router.get("/getActiveState", async (req, res) => {
  try {
    let allState: any = await getActiveState();
    sendEncryptedResponse(res,  allState, "Get all states");

  } catch (error) {
    serverError(res, error);
  }
});

//Get all states for dropdown
router.get("/getAllActiveState/:id", async (req, res) => {
  try {
    let state: any = await getAllActiveState(req.params);
    sendEncryptedResponse(res,  state, "Get Data successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//addState
router.post("/addState", tokenMiddleWare,async (req:any, res:any) => {
  let t = await dbInstance.transaction();
  try {
    const nameData = await checkStateName(req.body, req.params);
    if (nameData) {
      return alreadyExist(res, "This State Already Exist");
    }
    let user = req.user;
    let lastID: any = await State.findOne({
      order: [["id", "DESC"]],
      limit: 1,
    });

    lastID = lastID.get({raw:true});
    let latestID = lastID.id + 1;
    req.body = { ...req.body, id: latestID };
    let state: any = await addState(req.body, t);
    await t.commit();
    // let state: any = await addState(req.body,user);
    sendEncryptedResponse(res,  state, "New State is Added");

  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

//get by id
router.get("/getStatebyID/:id", async (req, res) => {
  try {
    let state: any = await getStatebyID(req.params);
    sendEncryptedResponse(res,  state, "Get Data Successfully by ID");

  } catch (error) {
    serverError(res, error);
  }
});

//updateState
router.put("/updateState/:id", tokenMiddleWare,async (req:any, res:any) => {
  let t = await dbInstance.transaction();
  try {
    const nameData = await checkStateName(req.body, req.params);

    if (nameData) {
      return alreadyExist(res, "This State Already Exist");
    }
    let user = req.user;
    let state: any = await updateState(req.body, req.params, t);
    await t.commit();
    sendEncryptedResponse(res,  state, "Data updated successfully");

  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

//deleteState
router.delete("/deleteState/:id", async (req, res) => {
  try {
    let state: any = await deleteState(req.params);
    sendEncryptedResponse(res,  state, "Data Deleted successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//Dump States
router.post("/dumpStates", async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let states: any = await dumpAllStates(req.body, t);
    await t.commit();
    success(res, states, "New States is Added");
  } catch (error) {
    console.log(error, "this si error");

    await t.rollback();
    serverError(res, error);
  }
});

module.exports = router;
