const { MakeQuery } = require("../../services/model-service");
import { Op } from "sequelize";
import { State, Country } from "../../db/core/init-control-db";

export const getActiveState = async () => {

    return State.findAll({
        where: { isActive: 1 },
    });

};


//for get state filter
export const getAllState = (query: any) => {
    const { limit, offset, modelOption, orderBy, attributes, forExcel } = MakeQuery({
        query: query,
        Model: State,
    });

    if (forExcel) {

        return State.findAll({
            where: modelOption,
            attributes,
            include: [
                {
                    model: Country,
                    as: 'country',
                    attributes: ["id", "countryName"],
                },
            ],
            order: orderBy,
            raw: true,
        });

    } else {
        modelOption.push({ isActive: 1 })
        return State.findAndCountAll({
            where: modelOption,
            attributes,
            include: [
                {
                    model: Country,
                    as: 'country',
                    attributes: ["id", "countryName"],
                },
            ],
            order: orderBy,
            raw: true,
            limit,
            offset,
        });
    }
};

//for get all state for dropdown
export const getAllActiveState = (params: any) => {
    return State.findAll({
        where: { countryId: params.id, isActive: 1 },
        include: [
            {
                model: Country,
                as: 'country',
                attributes: ['id', "countryName"]
            }
        ],
        raw: true,
        nest: true,
    });
}

//create state
export const addState = async (body: any, t: any) => {
    return State.create(body, {transaction: t});

};

//for update state
export const updateState = (body: any, params: any, t: any) => {
    return State.update(body, { where: { id: params.id }, transaction: t });
};


//for delete state
export const deleteState = (params: any) => {
    return State.destroy({
        where: {
            id: params.id,
        },
    });
};

//for get by id
export const getStatebyID = (params: any) => {
    return State.findOne({
        where: {
            id: params.id,
        },
    });
};

//for check unique state name
export const checkStateName = (body: any, params: any) => {
    const { stateName } = body;

    //for update time
    if (params.id) {
        return State.findOne({
            where: { stateName, id: { [Op.ne]: params.id } },
            raw: true,
        });

        // for add time
    } else {
        return State.findOne({
            where: { stateName },
            raw: true,
        });
    }
}


export const dumpAllStates = async (body: any, t: any) => {
    return State.bulkCreate(body, { transaction: t });
};