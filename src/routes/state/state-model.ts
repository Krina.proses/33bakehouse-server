import {
    Association,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    BelongsToCreateAssociationMixin,
    CreationOptional,
    DataTypes,
    HasManyGetAssociationsMixin,
    HasManySetAssociationsMixin,
    HasManyAddAssociationMixin,
    HasManyAddAssociationsMixin,
    HasManyCreateAssociationMixin,
    HasManyRemoveAssociationMixin,
    HasManyRemoveAssociationsMixin,
    HasManyHasAssociationMixin,
    HasManyHasAssociationsMixin,
    HasManyCountAssociationsMixin,
    InferCreationAttributes,
    InferAttributes,
    Model,
    NonAttribute,
    Sequelize
  } from 'sequelize'
  import type { City } from '../city/city-model'
  import type { Country } from '../country/country-model'
  
  type StateAssociations = 'country' | 'cities'
  
  
  
  
  export class State extends Model<any> {
    declare id: CreationOptional<number>
    declare stateName: string | null
    declare countryId: number
    declare isActive: boolean | null
    declare createdBy: number | null
    declare modifiedBy: number | null
    declare createdAt: CreationOptional<Date>
    declare updatedAt: CreationOptional<Date>
  
    // State belongsTo Country
    declare country?: NonAttribute<Country>
    declare getCountry: BelongsToGetAssociationMixin<Country>
    declare setCountry: BelongsToSetAssociationMixin<Country, number>
    declare createCountry: BelongsToCreateAssociationMixin<Country>
    
    // State hasMany City
    declare cities?: NonAttribute<City[]>
    declare getCities: HasManyGetAssociationsMixin<City>
    declare setCities: HasManySetAssociationsMixin<City, number>
    declare addCity: HasManyAddAssociationMixin<City, number>
    declare addCities: HasManyAddAssociationsMixin<City, number>
    declare createCity: HasManyCreateAssociationMixin<City>
    declare removeCity: HasManyRemoveAssociationMixin<City, number>
    declare removeCities: HasManyRemoveAssociationsMixin<City, number>
    declare hasCity: HasManyHasAssociationMixin<City, number>
    declare hasCities: HasManyHasAssociationsMixin<City, number>
    declare countCities: HasManyCountAssociationsMixin
    
    declare static associations: {
      country: Association<State, Country>,
      cities: Association<State, City>
    }
  
    static initModel(sequelize: Sequelize): typeof State {
      State.init({
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true
        },
        stateName: {
          type: DataTypes.STRING(255),
          
        },
        countryId: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        stateCode:{
          type: DataTypes.STRING(255),
          allowNull: false
        },
        isActive: {
          type: DataTypes.BOOLEAN
        },
        createdBy: {
          type: DataTypes.INTEGER
        },
        modifiedBy: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      }, {
        sequelize
      })
      
      return State
    }
  }
  