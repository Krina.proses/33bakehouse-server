import express from "express";
import prosesjwt from "proses-jwt";
import { createCusCart, deletecartdataByID, findData, getUserCartList, updateCart, updateQuantity } from "./cart-handler";
import { serverError, success } from "proses-response";
import { addToCartFunc } from "../../utils/helper";
let { tokenMiddleWare } = prosesjwt;
import dbInstance from "../../db/core/control-db";
const router = express.Router();

router.post(
  "/user-addcart",
  // tokenMiddleWare,
  async (req: any, res: any) => {
    let t = await dbInstance.transaction();
    try {

      let userId = req.body.userId;

      let cart: any =  await addToCartFunc(req.body, userId, t);
      console.log(cart,"cart");
      
      // const cartdata: any = await findData({ itemId }, userId);

      // let cart;

      // if (cartdata) {
      //   let ID = Number(cartdata.id);

      //   let updatedData = {
      //     quantity: cartdata.quantity + Number(req.body.quantity),
      //     amount: cartdata.amount + Number(req.body.amount),
      //     modifiedBy: req.user.id
      //   };

      //   await updateQuantity(updatedData, ID);
      // } else {
      //   let insertCart = {};

      //   insertCart = {
      //     ...req.body,
      //     isActive: 1,
      //     //@ts-ignore
      //     userId: req.user.id,
      //     createdBy: req.user.id
      //   };
      //   await createCusCart(insertCart);
      // }
      await t.commit();
      success(res, cart, "successfully added to cart");
    } catch (error) {        
      await t.rollback();
      serverError(res, error);
    }
  }
);

router.get(
  "/user-getCartByUserId",
  tokenMiddleWare,
  async (req: any, res: any) => {
    try {
      //@ts-ignore
      let userCart = await getUserCartList(req.user);
      
      // userCart = userCart.map((item: any) => item.get({ plain: true }))
      // // return;

      // let cartIndex = 0;
      // while (cartIndex < userCart.length) {

      //   if (userCart[cartIndex].Item) {
      //     let serialWiseItems = await findAllSerialNoByItem(userCart[cartIndex].Item.id, false);
      //     userCart[cartIndex]['serialNoCount'] = serialWiseItems.length;

      //     // if (serialWiseItems.length <= 0 || userCart[cartIndex].Item.acceptedQty <= 0) {
      //     // if (userCart[cartIndex].Item.acceptedQty <= 0) {
      //     //   userCart[cartIndex]['disable'] = true;
      //     // }

      //   }

      //   cartIndex++;
      // }



      success(res, userCart, "Got Data successfully");
    } catch (error) {
      console.log();
      
      serverError(res, error);
    }
  }
);

//update cart Qty
router.put(
  "/user-updateCartQty/:id",
  tokenMiddleWare,
  async (req: any, res: any) => {
    try {
      let data = await updateCart(req.body, req.params);

      success(res, data, "Data is updated");
    } catch (error) {
      serverError(res, error);
    }
  }
);

//delete cart item
router.delete(
  "/user-deleteCartItems/:id",
  tokenMiddleWare,
  async (req: any, res: any) => {
    try {
      let cart = await deletecartdataByID(req.params);
      success(res, cart, "Items Deleted successfully");
    } catch (error) {
      serverError(res, error);
    }
  }
);

module.exports = router;
