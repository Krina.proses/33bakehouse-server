import { CreationOptional, DataTypes, Model, Sequelize } from "sequelize";

export class Cart extends Model {
  declare id: CreationOptional<number>;
  declare userId: number;
  declare itemId: number;
  declare quantity: number;
  declare amount: number;
  declare isLogin: boolean;
  declare isActive: boolean;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {};

  static initModel(sequelize: Sequelize): typeof Cart {
    Cart.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        userId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        itemId: {
          type: DataTypes.INTEGER,
        },
        quantity: {
          type: DataTypes.INTEGER,
          defaultValue: 1
        },
        amount: {
          type: DataTypes.INTEGER,
        },
        isLogin: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return Cart;
  }
}
