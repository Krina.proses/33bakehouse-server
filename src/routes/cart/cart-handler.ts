import { Cart, Item, ItemImages } from "../../db/core/init-control-db";

export const createCusCart = (body: any, t: any) => {
  return Cart.create(body, {transaction: t});
};

//for addunique cart data
export const findData = ({ itemId }: any, userId: any) => {
  let where: { userId: number; itemId?: number } = { userId };
  if (itemId) {
    where.itemId = itemId;
  }
  return Cart.findOne({
    where,
    raw: true,
  });
};

export const updateQuantity = (body: any, id: number, t: any) => {
  return Cart.update(body, { where: { id }, transaction: t });
};

export const getUserCartList = (params: any) => {
  return Cart.findAll({
    where: { userId: params.id },
    include: [
      {
        model: Item,
        include: [
          {
            model: ItemImages,
            as: "itemImages",
            attributes: ["id", "itemId", "image"],
          },
        ],
        required: false
      },
    ],
  });
};

// update cart api for user
export const updateCart = (body: any, params: any) => {
  let { quantity, amount } = body;

  return Cart.update(
    { quantity, amount },
    { where: { id: Number(params.id) } }
  );
};

//delete cart items
export const deletecartdataByID = (params: any) => {
  return Cart.destroy({
    where: {
      id: params.id,
    },
  });
};

//for delete all cart item
export const deleteAllCartItem = (params: any) => {
  return Cart.destroy({
    where: {
      userId: params.id,
    }
  });
};
