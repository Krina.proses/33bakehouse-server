import {
  Association,
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  Sequelize,
} from "sequelize";
import { Category } from "../category/category-model";
import { ItemDetail } from "./itemDetail-model";

export class Item extends Model {
  declare id: CreationOptional<number>;
  declare categoryId: number;
  declare itemName: string;
  declare basicPrice: number;
  declare description: string;
  declare isActive: boolean;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {
    itemCategory: Association<Item, Category>;
    item: Association<Item, ItemDetail>
  };

  static initModel(sequelize: Sequelize): typeof Item {
    Item.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        categoryId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        itemName: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        basicPrice: {
          type: DataTypes.INTEGER,
        },
        description: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        createdBy: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
          allowNull: true,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return Item;
  }
}
