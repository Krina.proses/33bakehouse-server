import express from "express";
import prosesjwt from "proses-jwt";
import { alreadyExist, serverError, success } from "proses-response";
import dbInstance from "../../db/core/control-db";
import ErrorLogger from "../../db/core/logger/error-logger";
import { validate } from "../../middlewares/validation-middleware";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { v_params } from "../../validations/zod-params";
import {
  addItem,
  // bulkCreateItemDetail,
  bulkCreateItemImages,
  checkUniqueData,
  deleteItem,
  getAllActiveItem,
  getAllItemFilterWise,
  getItemByID,
  updateItem,
} from "./item-handler";
import { ItemDetail } from "./itemDetail-model";
import { itemImages } from "../../services/multer";
import { ItemImages } from "./itemImages-model";

let { tokenMiddleWare } = prosesjwt;

const router = express.Router();

//get All item (paginated)
router.get("/getAllItemFilterWise", tokenMiddleWare, async (req, res) => {
  try {
    let itemData = await getAllItemFilterWise(req.query);
    sendEncryptedResponse(res, itemData, "get All Item");
  } catch (error) {
    ErrorLogger.write({ type: "getAllItem error", error });
    serverError(res, error);
  }
});

// add item
router.post("/addItem", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let body = { ...req.body, createdBy: req.user.id };

    const newData: any = await checkUniqueData(body);
    if (newData) {
      throw alreadyExist(res, "This Item is already Exist.");
    }


    let itemData = await addItem(body, t);

    // let index = 0;
    // let attributeArray: any = [];
    // let attributeDetail = body.attributeDetail;

    // while (index < attributeDetail.length) {
    //   attributeArray.push({
    //     itemId: itemData.id,
    //     attributeId: attributeDetail[index].attributeId,
    //     attributeDetailId: attributeDetail[index].attributeDetailId,
    //     price: attributeDetail[index].price,
    //     isActive: attributeDetail[index].isActive,
    //     createdBy: req.user.id,
    //   });
    //   index++;
    // }

    // await bulkCreateItemDetail(attributeArray, t);

    await t.commit();

    sendEncryptedResponse(res, itemData, "Item added successfully");
  } catch (err) {
    await t.rollback();
    ErrorLogger.write({ type: "addItem error", err });
    serverError(res, err);
  }
});

// update item
router.put("/updateItem/:id", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let body = { ...req.body, modifiedBy: req.user.id };

    const newData: any = await checkUniqueData(body, req.params);
    if (newData) {
      throw alreadyExist(res, "This Item is already Exist.");
    }



    let itemData = await updateItem(body, req.params, t);

    // const id = req.params.id;
    // let deleteData = await ItemDetail.destroy({
    //   where: { itemId: id },
    // });

    // let index = 0;
    // let attributeArray: any = [];
    // let attributeDetail = body.attributeDetail;

    // while (index < attributeDetail.length) {
    //   const imagefile = req.files?.find((item: any) => {
    //     return item.originalname == attributeDetail[index].image;
    //   });

    //   attributeArray.push({
    //     itemId: id,
    //     attributeId: attributeDetail[index].attributeId,
    //     attributeDetailId: attributeDetail[index].attributeDetailId,
    //     price: attributeDetail[index].price,
    //     isActive: attributeDetail[index].isActive,
    //     modifiedBy: req.user.id,
    //   });
    //   index++;
    // }

    // await bulkCreateItemDetail(attributeArray, t);

    await t.commit();

    // success(res, itemData, "Item updated successfully");
    sendEncryptedResponse(res, itemData, "Item updated successfully");
  } catch (err) {
    await t.rollback();
    ErrorLogger.write({ type: "updateItem error", err });
    serverError(res, err);
  }
});

//get by id
router.get(
  "/getItemByID/:id",
  validate({ params: v_params }),
  tokenMiddleWare,
  async (req: any, res) => {
    try {
      let itemData: any = await getItemByID(req.params);

      sendEncryptedResponse(res, itemData, "Get Data Successfully by ID");
    } catch (error) {
      
      ErrorLogger.write({ type: "getItemByID error", error });
      serverError(res, error);
    }
  }
);

//get All Active item for dropdown
router.get("/getAllActiveItem", tokenMiddleWare, async (req, res) => {
  try {
    let itemData = await getAllActiveItem();
    sendEncryptedResponse(res, itemData, "Get Data successfully");
  } catch (error) {
    ErrorLogger.write({ type: "getAllActiveItem error", error });
    serverError(res, error);
  }
});

// add item images
router.put("/updateItemImages/:id", tokenMiddleWare, itemImages.array("FILES"), async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {

    let id = req.params.id

    let oldImages = JSON.parse(req.body.old);
    
    let deleteData = await ItemImages.destroy({
      where: { itemId: id },
    });

    let itemImages = req.files

    let index = 0;
    let itemImagesArray: any = [];

    for (let item of itemImages) {
      itemImagesArray.push({
        itemId: Number(id),
        image: item ? item.filename : null,
        modifiedBy:req.user.id, 
      });
    }

    let arr = [ ...itemImagesArray, ...oldImages.image]
        
    await bulkCreateItemImages(arr, t);
    
    await t.commit();

    sendEncryptedResponse(res, itemImagesArray, "Item added successfully");
  } catch (err) {
    
    await t.rollback();
    ErrorLogger.write({ type: "addItem error", err });
    serverError(res, err);
  }
});

router.delete(
  "/deleteItem/:id",
  tokenMiddleWare,
  async (req: any, res) => {
    let t = await dbInstance.transaction();
    try {
      let attribute: any = await deleteItem(req.params, t);
      await t.commit();
      sendEncryptedResponse(res, attribute, "Data deleted succesfully");
    } catch (error) {
      await t.rollback();
      ErrorLogger.write({ type: "delete attribute error", error });
      serverError(res, error);
    }
  }
);


// router.get(
//   "/getItemByCategoryId/:categoryId",
//   tokenMiddleWare,
//   async (req, res) => {
//     try {
//       let itemData = await getItemByCategoryId(req.params);
//       // success(res, itemData, "Get Data successfully");
//       sendEncryptedResponse(res, itemData, "Get Data successfully");
//     } catch (error) {
//       ErrorLogger.write({ type: "getItemByCategoryId error", error });
//       serverError(res, error);
//     }
//   }
// );

module.exports = router;
