import { CreationOptional, DataTypes, Model, Sequelize } from "sequelize";

export class ItemImages extends Model {
  declare id: CreationOptional<number>;
  declare itemId: number;
  declare image: string;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {};

  static initModel(sequelize: Sequelize): typeof ItemImages {
    ItemImages.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        itemId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        image: {
          type: DataTypes.STRING(255),
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return ItemImages;
  }
}
