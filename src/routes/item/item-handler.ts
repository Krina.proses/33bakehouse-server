import {
  Item,
  Category,
  // ItemDetail,
  // Attribute,
  // AttributeDetail,
  ItemImages,
} from "../../db/core/init-control-db";
import { Op, Transaction, where } from "sequelize";
const { MakeQuery } = require("../../services/model-service");

// import { MakeQuery } from "../../services/model-service";

// for add new item
export const addItem = (body: any, t: Transaction) => {
  return Item.create(body, { transaction: t });
};

// export const bulkCreateItemDetail = (body: any, t: Transaction) => {
//   return ItemDetail.bulkCreate(body, { transaction: t });
// };

export const bulkCreateItemImages = (body: any, t: Transaction) => {
  return ItemImages.bulkCreate(body, { transaction: t });
};

//for update item
export const updateItem = (body: any, params: any, t: any) => {
  return Item.update(body, {
    where: { id: params.id },
    transaction: t,
  });
};

// for get by id - item
export const getItemByID = (params: any) => {
  return Item.findOne({
    where: {
      id: params.id,
    },
    include: [
      // {
      //   model: ItemDetail,
      //   attributes: [ "id", "itemId", "attributeId", "attributeDetailId", "price" ]
      //   // include: [
      //   //   {
      //   //     model: AttributeDetail,
      //   //     include: [
      //   //       {
      //   //         model: Attribute,
      //   //       },
      //   //     ],
      //   //   },
      //   // ],
      // },
      {
        model: ItemImages,
        as: "itemImages",
        attributes: [ "id", "itemId", "image" ]
      },
      {
        model: Category,
        attributes: [ "id", "title" ]
      }
    ],
  });
};

//for get all item for dropdown
export const getAllActiveItem = () => {
  return Item.findAll({
    where: { isActive: 1 },
  });
};

//for get item filter
export const getAllItemFilterWise = async (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } =
    MakeQuery({
      query: query,
      Model: Item,
    });

    let includeOption = [
      {
        model: Category,
        attributes: ["id", "title"],
      },
      {
        model: ItemImages,
        as: "itemImages",
        attributes: [ "id", "itemId", "image" ]
      },
    ]

  if (forExcel) {
    return Item.findAll({
      where: modelOption,
      attributes,
      include: includeOption,
      order: orderBy,
      // raw: true,
    });
  } else {
    return Item.findAndCountAll({
      where: modelOption,
      include: includeOption,
      attributes,
      order: orderBy,
      limit,
      subQuery: false,
      distinct: true,
      // raw: true,
      offset,
    });
  }
};

//for check unique item title
export const checkItemData = (title: any, params?: any) => {
  //for update time
  if (params) {
    return Item.findOne({
      where: { title, id: { [Op.ne]: params.id } },
    });

    // for add time
  } else {
    return Item.findOne({
      where: { title },
    });
  }
};

export const checkUniqueData = (body: any, params?: any) => {
  const { itemName, id } = body;
  //for update time
  if (params) {
    return Item.findOne({
      where: { itemName, id: { [Op.ne]: params.id }, isActive: 1 },
    });

    // for add time
  } else {
    return Item.findOne({
      where: { itemName, isActive: 1 },
    });
  }
};

// export const findItemDetail = (id: number) => {
//   return ItemDetail.findAll({
//     where: {
//       attributeId: id
//     }
//   });
// };

// export const deleteItemDetail = (ids: any) => {
//   return ItemDetail.destroy({
//     where: {
//       id: ids
//     }
//   });
// };

export const deleteItem = (params: any, t: any) => {
  return Item.destroy({
    where: {
      id: params.id,
    },
    transaction: t,
  });
};