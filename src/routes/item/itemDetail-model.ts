import { Association, CreationOptional, DataTypes, Model, Sequelize } from "sequelize";
import { Attribute } from "../attribute/attribute-model";
import { AttributeDetail } from "../attribute/attributeDetail-model";
import { Item } from "./item-model";

export class ItemDetail extends Model {
  declare id: CreationOptional<number>;
  declare itemId: number;
  declare attributeId: number;
  declare attributeDetailId: number;
  declare price: number;
  declare isActive: boolean;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {
    item: Association<ItemDetail, Item>
    attribute: Association<ItemDetail, Attribute>;
    attributeDetail: Association<ItemDetail, AttributeDetail>;
  };

  static initModel(sequelize: Sequelize): typeof ItemDetail {
    ItemDetail.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        itemId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        attributeId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        attributeDetailId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        price: {
          type: DataTypes.INTEGER,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return ItemDetail;
  }
}
