// import { MakeQuery } from "../../services/model-service";
// import { Attribute, AttributeDetail } from "../../db/core/init-control-db";
// import { Op } from "sequelize";

// export const getAllAttributeFilterWise = async (query: any) => {
//   const { limit, offset, modelOption, orderBy, attributes, forExcel } =
//     MakeQuery({
//       query: query,
//       Model: Attribute,
//     });

//   if (forExcel) {
//     return Attribute.findAll({
//       where: modelOption,
//       attributes,
//       order: orderBy,
//       raw: true,
//     });
//   } else {
//     return Attribute.findAndCountAll({
//       where: modelOption,
//       attributes,
//       order: orderBy,
//       limit,
//       raw: true,
//       offset,
//     });
//   }
// };

// export const addAttribute = (body: any, t: any) => {
//   return Attribute.create(body, { transaction: t });
// };

// export const bulkCreateAttributeDetail = (body: any, t: any) => {
//   return AttributeDetail.bulkCreate(body, { transaction: t });
// };

// export const updateAttribute = (body: any, params: any, t: any) => {
//   return Attribute.update(body, { where: { id: params.id }, transaction: t });
// };

// export const getAttributeById = (params: any) => {
//   return Attribute.findOne({
//     where: {
//       id: params.id,
//     },
//     include: [
//       {
//         model: AttributeDetail,
//         attributes: [ "id", "attributeId", "value" ]
//       },
//     ],
//   });
// };

// export const deleteAttribute = (params: any, t: any) => {
//   return Attribute.destroy({
//     where: {
//       id: params.id,
//     },
//     transaction: t,
//   });
// };

// export const getAttrDetbyAttributeId = (params: any) => {
//   return AttributeDetail.findAll({
//     where: {
//       attributeId: params.id,
//       isActive: 1,
//     },
//     include: [
//       {
//         model: Attribute,
//         attributes: [ "id", "attributeName", "displayName" ]
//       },
//     ],
//   });
// };

// export const getActiveAttribute = () => {
//   return Attribute.findAll({
//     where: {
//       isActive: 1,
//     },
//   });
// };

// export const checkUniqueData = (body: any, params?: any) => {
//   const { attributeName, displayName, id } = body;
//   //for update time
//   if (params) {
//     return Attribute.findOne({
//       where: {
//         [Op.or]: [{ attributeName }, { displayName }],
//         id: { [Op.ne]: params.id },
//         isActive: 1,
//       },
//     });

//     // for add time
//   } else {
//     return Attribute.findOne({
//       where: { [Op.or]: [{ attributeName }, { displayName }], isActive: 1 },
//     });
//   }
// };

// export const CreateAttributeDetail = (body: any, t: any) => {
//   return AttributeDetail.create(body, { transaction: t });
// };

// export const updateAttributeDetail = (body: any, id: any, t: any) => {
//   return AttributeDetail.update(body, { where: { id: id }, transaction: t });
// };

// export const deleteAttributeDetail = (params: any, t: any) => {
//   return AttributeDetail.destroy({
//     where: {
//       id: params.id,
//     },
//     transaction: t,
//   });
// };
