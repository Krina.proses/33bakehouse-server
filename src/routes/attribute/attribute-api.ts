// import express from "express";
// import { alreadyExist, serverError, success } from "proses-response";
// import prosesjwt from "proses-jwt";
// import { sendEncryptedResponse } from "../../services/encryptResponse-service";
// import dbInstance from "../../db/core/control-db";
// import ErrorLogger from "../../db/core/logger/error-logger";
// import {
//   CreateAttributeDetail,
//   addAttribute,
//   bulkCreateAttributeDetail,
//   checkUniqueData,
//   deleteAttribute,
//   deleteAttributeDetail,
//   getActiveAttribute,
//   getAllAttributeFilterWise,
//   getAttrDetbyAttributeId,
//   getAttributeById,
//   updateAttribute,
//   updateAttributeDetail,
// } from "./attribute-handler";
// import { attributeImages } from "../../services/multer";
// import { AttributeDetail } from "./attributeDetail-model";
// import { deleteItemDetail, findItemDetail } from "../item/item-handler";
// let { tokenMiddleWare, generateToken } = prosesjwt;

// const router = express.Router();

// router.get(
//   "/getAllAttributeFilterWise",
//   tokenMiddleWare,
//   async (req: any, res) => {
//     try {
//       let allattribute: any = await getAllAttributeFilterWise(req.query);
//       sendEncryptedResponse(res, allattribute, "got all attribute");
//     } catch (error) {
//       ErrorLogger.write({ type: "getAllAttribute error", error });
//       serverError(res, Error);
//     }
//   }
// );

// router.post(
//   "/addAttribute",
//   tokenMiddleWare,
//   attributeImages.array("FILES"),
//   async (req: any, res) => {
//     let t = await dbInstance.transaction();
//     try {
//       const data = JSON.parse(req.body.formData);
//       data.createdBy = req.user.id;

//       const newData: any = await checkUniqueData(data);
//       if (newData) {
//         if (newData.attributeName == data.attributeName) {
//           throw alreadyExist(res, "This Attribute Name is already Exist.");
//         }
//         if (newData.displayName == data.displayName) {
//           throw alreadyExist(res, "This Display Name is already Exist.");
//         }
//       }

//       let attributeData: any = await addAttribute(data, t);

//       let index = 0;
//       let attributeArray: any = [];
//       let attributeDetail = data.attributeDetail;

//       while (index < attributeDetail.length) {
//         const imagefile = req.files?.find((item: any) => {
//           return item.originalname == attributeDetail[index].image;
//         });

//         attributeArray.push({
//           attributeId: attributeData.id,
//           value: attributeDetail[index].value,
//           description: attributeDetail[index].description,
//           image: imagefile ? imagefile.filename : attributeDetail[index].image,
//           isActive: attributeDetail[index].isActive,
//           createdBy: req.user.id,
//         });
//         index++;
//       }

//       await bulkCreateAttributeDetail(attributeArray, t);

//       await t.commit();
//       sendEncryptedResponse(
//         res,
//         attributeData,
//         "New Attribute Added Successfully"
//       );
//     } catch (error) {
//       await t.rollback();
//       ErrorLogger.write({ type: "getAllAttribute error", error });
//       serverError(res, Error);
//     }
//   }
// );

// router.put(
//   "/updateAttribute/:id",
//   tokenMiddleWare,
//   attributeImages.array("FILES"),
//   async (req: any, res) => {
//     let t = await dbInstance.transaction();
//     try {
//       const data = JSON.parse(req.body.formData);
//       data.modifiedBy = req.user.id;
//       console.log(data, "datadata");

//       const newData: any = await checkUniqueData(data, req.params);
//       if (newData) {
//         if (newData.attributeName == data.attributeName) {
//           throw alreadyExist(res, "This Attribute Name is already Exist.");
//         }
//         if (newData.displayName == data.displayName) {
//           throw alreadyExist(res, "This Display Name is already Exist.");
//         }
//       }

//       let updateData: any = await updateAttribute(data, req.params, t);

//       const id = req.params.id;

//       // let deleteData = await AttributeDetail.destroy({
//       //   where: { attributeId: id },
//       // });

//       let index = 0;
//       let attributeArray: any = [];
//       let attributeDetail = data.attributeDetail;

//       for (let item of attributeDetail) {
//         const imagefile = req.files?.find((item: any) => {
//           return item.originalname == item.image;
//         });

//         if (item.id) {

//           item = { ...item, modifiedBy: req.user.id }
//           await updateAttributeDetail(item, item.id, t);
//         } else {
//           let tempObj = {
//             attributeId: id,
//             value: item.value,
//             description: item.description,
//             image: imagefile ? imagefile.filename : item.image,
//             isActive: item.isActive,
//             modifiedBy: req.user.id,
//           };

//           attributeArray.push(tempObj);

//           await bulkCreateAttributeDetail(attributeArray, t);
//         }
//         // return;

//         //   let tempObj = {
//         //     attributeId: id,
//         //     value: item.value,
//         //     description: item.description,
//         //     image: imagefile ? imagefile.filename : item.image,
//         //     isActive: item.isActive,
//         //     modifiedBy:req.user.id,
//         //   }

//         //   attributeArray.push(tempObj);

//         // await CreateAttributeDetail(attributeArray, t);
//       }

//       // while (index < attributeDetail.length) {

//       //   const imagefile = req.files?.find((item: any) => {
//       //     return item.originalname == attributeDetail[index].image;
//       //   });

//       //   attributeArray.push({
//       //     attributeId: id,
//       //     value: attributeDetail[index].value,
//       //     description: attributeDetail[index].description,
//       //     image: imagefile ? imagefile.filename : attributeDetail[index].image,
//       //     isActive: attributeDetail[index].isActive,
//       //     modifiedBy:req.user.id,
//       //   });
//       //   index++;
//       // }

//       // await bulkCreateAttributeDetail(attributeArray, t);

//       await t.commit();
//       sendEncryptedResponse(res, updateData, "Update Attribute Successfully");
//     } catch (error) {
//       console.log(error, "errorrrrrrrrrrrr");

//       await t.rollback();
//       ErrorLogger.write({ type: "getAllAttribute error", error });
//       serverError(res, Error);
//     }
//   }
// );

// router.get("/getAttributeById/:id", tokenMiddleWare, async (req: any, res) => {
//   try {
    
//     let data: any = await getAttributeById(req.params);
//     sendEncryptedResponse(res, data, "got attribute by id");
//   } catch (error) {
//     ErrorLogger.write({ type: "getAllAttribute error", error });
//     serverError(res, Error);
//   }
// });

// router.get("/getActiveAttribute", tokenMiddleWare, async (req: any, res) => {
//   try {
//     let data: any = await getActiveAttribute();
//     sendEncryptedResponse(res, data, "got All attribute");
//   } catch (error) {
//     ErrorLogger.write({ type: "getAllAttribute error", error });
//     serverError(res, Error);
//   }
// });

// //get by id
// router.get("/getAttrDetbyAttributeId/:id", async (req, res) => {
//   try {
//     let attriData: any = await getAttrDetbyAttributeId(req.params);
//     sendEncryptedResponse(res, attriData, "Get Data Successfully by ID");
//   } catch (error) {
//     serverError(res, error);
//   }
// });

// router.delete(
//   "/deleteAttribute/:id",
//   tokenMiddleWare,
//   async (req: any, res) => {
//     let t = await dbInstance.transaction();
//     try {
//       let attribute: any = await deleteAttribute(req.params, t);
//       await t.commit();
//       sendEncryptedResponse(res, attribute, "Data deleted succesfully");
//     } catch (error) {
//       await t.rollback();
//       ErrorLogger.write({ type: "delete attribute error", error });
//       serverError(res, error);
//     }
//   }
// );

// router.delete(
//   "/deleteAttributeDetail/:id",
//   tokenMiddleWare,
//   async (req: any, res) => {
//     let t = await dbInstance.transaction();
//     try {
//       let attribute: any = await deleteAttributeDetail(req.params, t);
//       await t.commit();
//       sendEncryptedResponse(res, attribute, "Data deleted succesfully");
//     } catch (error) {
//       await t.rollback();
//       ErrorLogger.write({ type: "delete attribute error", error });
//       serverError(res, error);
//     }
//   }
// );

// module.exports = router;
