import {
  Association,
  CreationOptional,
  DataTypes,
  Model,
  Sequelize,
} from "sequelize";
import { ItemDetail } from "../item/itemDetail-model";

export class Attribute extends Model {
  declare id: CreationOptional<number>;
  declare attributeName: string;
  declare displayName: string;
  declare isActive: boolean;
  declare modifiedBy: number | null;
  declare createdBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  declare static associations: {
    attribute: Association<Attribute, ItemDetail>;
 
  };

  static initModel(sequelize: Sequelize): typeof Attribute {
    Attribute.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true,
        },
        attributeName: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        displayName: {
          type: DataTypes.STRING(255),
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return Attribute;
  }
}
