import {
  Association,
  CreationOptional,
  DataTypes,
  Model,
  Sequelize,
  TextDataType,
} from "sequelize";
import { ItemDetail } from "../item/itemDetail-model";

export class AttributeDetail extends Model {
  declare id: CreationOptional<number>;
  declare attributeId: number;
  declare value: string;
  declare description: TextDataType;
  declare image: string;
  declare isActive: boolean;
  declare modifiedBy: number | null;
  declare createdBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  declare static associations: {
    attributeDetail: Association<AttributeDetail, ItemDetail>
  };

  static initModel(sequelize: Sequelize): typeof AttributeDetail {
    AttributeDetail.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true,
        },
        attributeId: {
          type: DataTypes.INTEGER,
        },
        value: {
          type: DataTypes.STRING(255),
        },
        description: {
          type: DataTypes.TEXT,
        },
        image: {
          type: DataTypes.STRING(255),
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return AttributeDetail;
  }
}
