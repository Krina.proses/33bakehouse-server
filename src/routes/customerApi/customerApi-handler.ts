import {
  Cart,
  Category,
  Item,
  ItemImages,
  User,
  UserAddress,
} from "../../db/core/init-control-db";
import { Op } from "sequelize";

export const addRegisterData = (body: any, t: any) => {
  return User.create(body, { transaction: t });
};

//for check unique customer name
export const checkCustomerByName = (body: any) => {
  const { email, id, socialLogin } = body;

  //for update time
  if (id) {
    return User.findOne({
      where: {
        // email,
          [Op.or]: [{ email }, { socialLogin }],
        id: { [Op.ne]: id },
      },
      raw: true,
    });

    // for add time
  } else {
    return User.findOne({
      where: {
        email,
        // [Op.or]: [{ email }, { phoneNumber }]
      },
      raw: true,
    });
  }
};

export const getAllActiveCategory = () => {
  return Category.findAll({
    where: {
      isActive: 1,
    },
  });
};

export const getAllActiveProduct = () => {
  return Item.findAll({
    where: {
      isActive: 1,
    },
    include: [
      {
        model: ItemImages,
        as: "itemImages",
        attributes: ["id", "itemId", "image"],
      },
    ],
  });
};

export const getCategoryByID = (params: any) => {
  return Item.findAll({
    where: {
      categoryId: params.id,
      isActive: 1,
    },
    include: [
      {
        model: ItemImages,
        as: "itemImages",
        attributes: ["id", "itemId", "image"],
      },
      {
        model: Category,
        attributes: [ "id", "title" ]
      }
    ],
  });
};

//get user cart count info
export const getCartCount = (userId: number) => {
  return Cart.count({
    where: [{ userId }],
  });
};

export const deleteUserAddress = (params: any, t: any) => {
  return UserAddress.destroy({
    where: {
      id: params.id
    },
    transaction: t
  })
}

export const updateUserAddress = (body: any, params: any, t: any) => {
  return UserAddress.update(body, {
    where: {
      id: params.id
    },
    transaction: t
  })
}
