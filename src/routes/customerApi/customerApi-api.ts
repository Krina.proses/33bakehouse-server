import express from "express";
import prosesjwt from "proses-jwt";
import {
  alreadyExist,
  other,
  serverError,
  success,
  unauthorized,
} from "proses-response";
import dbInstance from "../../db/core/control-db";
import { sendEmail } from "../../services/mailService";
import { GLOBAL_CONSTANTS } from "../../utils/constants";
import { addToCartFunc } from "../../utils/helper";
import { getItemByID } from "../item/item-handler";
import {
  checkPassword,
  checkUser,
  getUserByEmailSocialLogin,
  getUserByid,
  updateUser,
  updateuserPassword,
} from "../user/user-handler";
import { User } from "../user/user-model";
import {
  addRegisterData,
  deleteUserAddress,
  getAllActiveCategory,
  getAllActiveProduct,
  getCartCount,
  getCategoryByID,
  updateUserAddress,
} from "./customerApi-handler";

let { tokenMiddleWare, generateToken } = prosesjwt;

const router = express.Router();

router.post("/user-registration", async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {
    let { password, email, cartItems } = req.body;

    let user: any = await checkUser(email);

    // const nameData = await checkCustomerByName(req.body);
    // if (nameData) {
    //   throw alreadyExist(res, "Email already exist");
    // }
    // return

    req.body = {
      ...req.body,
      roleId: GLOBAL_CONSTANTS.roles.User,
      isActive: 1,
    };

    let userData: any;

    if (!user) {
      userData = await addRegisterData(req.body, t);
    } else {
      throw alreadyExist(res, "Email already exist");
    }

    let userId = user ? user.id : userData.id;

    if (cartItems) {
      if (cartItems?.length) {
        // @ts-ignore
        await cartItems.forEach(
          async (element: any) => await addToCartFunc(element, userId, t)
        );
      } else cartItems = [];
    }

    //send email
    const mailData: any = {
      to: email,
      // to: "krina.proses@gmail.com",
      subject: "33BakeHouse",
      html: `<div>
          Hello  ${req.body.name}<br/><br/>
          Welcome to 33 bakehouse website!! Continue with us by ordering delicious food. Hope you enjoy it!!<br/><br/>
          Regards,<br/>    
          33 BakeHouse          
         </div>`,
    };

    sendEmail(mailData);
    await t.commit();
    success(res, userData, "User Register successfully");
  } catch (err) {
    console.log(err, "errorrrrrrrrr");
    await t.rollback();
    serverError(res, err);
  }
});

router.post("/user-socialLogin", async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {
    let user: any = await getUserByEmailSocialLogin(
      req.body?.email,
      req.body?.socialLogin
    );

    req.body = {
      ...req.body,
      roleId: GLOBAL_CONSTANTS.roles.User,
      isActive: 1,
    };

    let addData: any;
    if (!user) {
      addData = await addRegisterData(req.body, t);
    }
    await t.commit();
    success(res, addData, "User Login successfully");
  } catch (err) {
    console.log("err", err);
    await t.rollback();
    serverError(res, err);
  }
});

router.post("/user-login", async (req: any, res: any) => {
  try {
    console.log(req.body, "bodyyyyyyyyyyy");

    let { password, email, cartArray } = req.body;

    let user: any = await checkUser(email);

    if (!user) {
      throw other(res, "Invalid credentials");
    }

    if (!checkPassword(password, user.password)) {
      return unauthorized(res, "Invalid Credentials");
    }

    let cartCount = await getCartCount(user.id);

    // return
    let userObj = {
      id: user.id,
      name: user.name,
      email: user.email,
      mobile: user.mobile,
      roleId: user.roleId,
      cartCount: cartCount,
    };

    const plainUser = user.get({ plain: true });

    const result: any = {
      id: plainUser.id,
      name: plainUser.name,
      email: plainUser.email,
      mobile: plainUser.mobile,
    };

    const token = await generateToken(result);
    console.log(token, "token");

    let data = {
      token,
      user: userObj,
    };
    success(res, data, "login successfull");
  } catch (error) {
    serverError(res, Error);
  }
});

//get all categories
router.get("/getAllActiveCategory", async (req: any, res: any) => {
  try {
    let category: any = await getAllActiveCategory();
    success(res, category, "got all categories");
  } catch (error) {
    serverError(res, Error);
  }
});

//get all product
router.get("/getAllActiveProduct", async (req: any, res: any) => {
  try {
    let product: any = await getAllActiveProduct();
    success(res, product, "got all products");
  } catch (error) {
    console.log(error, "errorrrrrrrrr");

    serverError(res, Error);
  }
});

router.get("/getCategoryByID/:id", async (req: any, res: any) => {
  try {
    let product: any = await getCategoryByID(req.params);
    success(res, product, "got all products");
  } catch (error) {
    console.log(error, "errorrrrrrrrr");

    serverError(res, Error);
  }
});

router.get("/getProductByID/:id", async (req: any, res) => {
  try {
    let itemData: any = await getItemByID(req.params);

    success(res, itemData, "Get Data Successfully by ID");
  } catch (error) {
    console.log(error, "errorrrrrrrrr");

    serverError(res, error);
  }
});

router.get("/getProfileData", tokenMiddleWare, async (req: any, res) => {
  try {
    let profile: any = await getUserByid(req.user.id);
    console.log(profile, "profile");

    success(res, profile, "Get Data Successfully by ID");
  } catch (error) {
    serverError(res, error);
  }
});

router.put("/updateProfile/:id", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let profile: any = await updateUser(req.body, req.params, t);
    await t.commit();
    success(res, profile, "Profile Update Successfully");
  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

router.delete(
  "/deleteUserAddress/:id",
  tokenMiddleWare,
  async (req: any, res: any) => {
    let t = await dbInstance.transaction();
    try {
      let address: any = await deleteUserAddress(req.params, t);
      await t.commit();
      success(res, address, "Data Deleted Successfully");
    } catch (error) {
      console.log(error, "errorrrrrrrrr");
      await t.rollback();
      serverError(res, error);
    }
  }
);

router.put("/updateUserAddress/:id", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let userAddress: any = await updateUserAddress(req.body, req.params, t);
    await t.commit();
    success(res, userAddress, "User Address Update Successfully");
  } catch (error) {
    console.log(error, "errorrrrrrrrr");
    await t.rollback();
    serverError(res, error);
  }
});

router.post("/ChangePassword", tokenMiddleWare, async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {
    let { oldPassword, newPassword } = req.body;
    let user: any = await User.findOne({
      //@ts-ignore-
      where: { id: req.user.id },
      attributes: ["email", "id", "name", "password"],
      raw: true,
    });

    if (!checkPassword(oldPassword, user.password)) {
      throw other(res, "Your Old Password Is Wrong");
    }
    if (checkPassword(newPassword, user.password)) {
      throw other(res, "Old Password and New Password Cannot be Same!");
    }

    //@ts-ignore-
    let newValue = await updateuserPassword(req.body, req.user.id, t);
    await t.commit();
    success(res, newValue, "Your Password Is Successfully Updated");
  } catch (error) {
    console.log(error, "errorrrrrrrrr");
    await t.rollback();
    serverError(res, error);
  }
});

module.exports = router;
