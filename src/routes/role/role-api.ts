import express from "express";
import prosesjwt from "proses-jwt";
import { alreadyExist, serverError } from "proses-response";
import dbInstance from "../../db/core/control-db";
import ErrorLogger from "../../db/core/logger/error-logger";
import { validate } from "../../middlewares/validation-middleware";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { v_params } from "../../validations/zod-params";
import {
  addRole,
  checkRoleName,
  deleteRole,
  getAllActiveRole,
  getAllRole,
  getAllRoles,
  getRolebyID,
  updateRole,
} from "./role-handler";
import { v_role } from "./role-validation";
let { tokenMiddleWare } = prosesjwt;
const router = express.Router();

//findAllRoles dropdown
router.get("/getRoles",
tokenMiddleWare, 
async (req, res) => {
  try {
    let allRoles: any = await getAllRoles();
    // success(res, allRoles, "get Roles");
    sendEncryptedResponse(res,  allRoles, "get Roles");
  } catch (error) {
    ErrorLogger.write({ type: "getRoles error", error });
    serverError(res, error);
  }
});

//findAllRoles
router.get("/getAllRoles", 
tokenMiddleWare,
async (req, res) => {
  try {
    let allRoles: any = await getAllRole(req.query);
    // success(res, allRoles, "login successfull");
    sendEncryptedResponse(res,  allRoles, "get All Roles");
  } catch (error) {
    ErrorLogger.write({ type: "getAllRoles error", error });
    serverError(res, error);
  }
});

//Get all role for dropdown
router.get("/getAllActiveRole", 
tokenMiddleWare,
async (req, res) => {
  try {
    let country: any = await getAllActiveRole();
    // success(res, country, "Get Data successfully");
    sendEncryptedResponse(res,  country, "Get Data successfully");
  } catch (error) {
    ErrorLogger.write({ type: "getAllActiveRole error", error });
    serverError(res, error);
  }
});

//get by id
router.get("/getRolebyID/:id", 
validate({params: v_params}), 
tokenMiddleWare,async (req, res) => {
  try {
    let role: any = await getRolebyID(req.params);
    // success(res, role, "Get Data Successfully by ID");
    sendEncryptedResponse(res,  role, "Get Data Successfully by ID");
  } catch (error) {
    ErrorLogger.write({ type: "getRolebyID error", error });
    serverError(res, error);
  }
});

//addrole
router.post("/addRole", 
validate({body: v_role}), 
tokenMiddleWare,async (req, res) => {
  let t = await dbInstance.transaction();

  try {
    //validates the request body
    const body = v_role.parse(req.body);
    const nameData = await checkRoleName(body);
    let role: any = await addRole(body, t);
    await t.commit();
    // success(res, role, "New Role is Added");
    sendEncryptedResponse(res,  role, "New Role is Added");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "addRole error", error });
    serverError(res, error);
  } 
});

//updateRole
router.put("/updateRole/:id", 
validate({body: v_role, params: v_params}), 
tokenMiddleWare,async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    const body = v_role.parse(req.body);
    const nameData = await checkRoleName(body, req.params);

    if (nameData) {
      throw alreadyExist(res, "This Role already exists");
    }

    let role: any = await updateRole(body, req.params, t);
    await t.commit();
    // success(res, role, "Data updated successfully");
    sendEncryptedResponse(res,  role, "Data updated successfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "updateRole error", error });
    serverError(res, error);
  }
});

//deleteRole
router.delete("/deleteRole/:id", 
tokenMiddleWare, 
async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let role: any = await deleteRole(req.params);
    await t.commit();
    // success(res, role, "Data Deleted successfully");
    sendEncryptedResponse(res,  role, "Data Deleted successfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "deleteRole error", error });
    serverError(res, error);
  }
});

module.exports = router;