import {
  Association,
  CreationOptional,
  DataTypes,
  HasManyGetAssociationsMixin,
  HasManySetAssociationsMixin,
  HasManyAddAssociationMixin,
  HasManyAddAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyRemoveAssociationMixin,
  HasManyRemoveAssociationsMixin,
  HasManyHasAssociationMixin,
  HasManyHasAssociationsMixin,
  HasManyCountAssociationsMixin,
  InferCreationAttributes,
  InferAttributes,
  Model,
  NonAttribute,
  Sequelize,
} from "sequelize";
import type { User } from "../user/user-model";

type RoleAssociations = "users";

export class Role extends Model<
  InferAttributes<Role, { omit: RoleAssociations }>,
  InferCreationAttributes<Role, { omit: RoleAssociations }>
> {
  declare id: CreationOptional<number>;
  declare roleName: string;
  declare permission: string;
  declare isActive: boolean ;
  declare createdBy: number;
  declare modifiedBy: number;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  // Role hasMany User
  declare users?: NonAttribute<User[]>;
  declare getUsers: HasManyGetAssociationsMixin<User>;
  declare setUsers: HasManySetAssociationsMixin<User, number>;
  declare addUser: HasManyAddAssociationMixin<User, number>;
  declare addUsers: HasManyAddAssociationsMixin<User, number>;
  declare createUser: HasManyCreateAssociationMixin<User>;
  declare removeUser: HasManyRemoveAssociationMixin<User, number>;
  declare removeUsers: HasManyRemoveAssociationsMixin<User, number>;
  declare hasUser: HasManyHasAssociationMixin<User, number>;
  declare hasUsers: HasManyHasAssociationsMixin<User, number>;
  declare countUsers: HasManyCountAssociationsMixin;

  declare static associations: {
    users: Association<Role, User>;
  };

  static initModel(sequelize: Sequelize): typeof Role {
    Role.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        roleName: {
          type: DataTypes.STRING(255),
          allowNull: false,
          unique: true,
        },
        permission: {
          type: DataTypes.TEXT,
       
          allowNull: true,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: true
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );

    return Role;
  }
}
