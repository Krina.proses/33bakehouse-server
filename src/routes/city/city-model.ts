import {
    Association,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    BelongsToCreateAssociationMixin,
    CreationOptional,
    DataTypes,
    HasManyGetAssociationsMixin,
    HasManySetAssociationsMixin,
    HasManyAddAssociationMixin,
    HasManyAddAssociationsMixin,
    HasManyCreateAssociationMixin,
    HasManyRemoveAssociationMixin,
    HasManyRemoveAssociationsMixin,
    HasManyHasAssociationMixin,
    HasManyHasAssociationsMixin,
    HasManyCountAssociationsMixin,
    InferCreationAttributes,
    InferAttributes,
    Model,
    NonAttribute,
    Sequelize
  } from 'sequelize'
  import type { State } from '../state/state-model'
  
  type CityAssociations =  'state' | 'users' | 'donations'
  
  export class City extends Model<any> {
    declare id: CreationOptional<number>
    declare cityName: string | null
    declare stateId: number
    declare isActive: boolean | null
    declare createdDate: string | null
    declare modifiedDate: string | null
    declare createdAt: CreationOptional<Date>
    declare updatedAt: CreationOptional<Date>
  
    
    // City belongsTo State
    declare state?: NonAttribute<State>
    declare getState: BelongsToGetAssociationMixin<State>
    declare setState: BelongsToSetAssociationMixin<State, number>
    declare createState: BelongsToCreateAssociationMixin<State>
    

  
  



    
    declare static Associations: {
      state: Association<City, State>,
   
    }
  
    static initModel(sequelize: Sequelize): typeof City {
      City.init({
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true
        },
        cityName: {
          type: DataTypes.STRING
        },
        stateId: {
          type: DataTypes.INTEGER,
          allowNull: false
        },
        zoneId: {
          type: DataTypes.INTEGER,
          allowNull: true
        },
        isActive: {
          type: DataTypes.BOOLEAN
        },
        createdBy: {
          type: DataTypes.INTEGER
        },
        modifiedBy: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      }, {
        sequelize
      })
      
      return City
    }
  }
  