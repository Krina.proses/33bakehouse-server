import express from "express";
import {
  addCity,
  checkCityName,
  deleteCity,
  getAllActiveCity,
  getAllCity,
  getCitybyID,
  updateCity,
  dumpAllCities,
  getAllCityDropdown
} from "./city-handler";
import { alreadyExist, serverError, success } from "proses-response";
import prosesjwt from "proses-jwt";
const { tokenMiddleWare } = prosesjwt;
const router = express.Router();
import dbInstance from "../../db/core/control-db";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { City } from "../../db/core/init-control-db";

//findAllCity
router.get("/getAllCity", async (req, res) => {
  try {
    let allCity: any = await getAllCity(req.query);
    sendEncryptedResponse(res,  allCity, "Get all cities");

  } catch (error) {
    serverError(res, error);
  }
});

//Get all city for dropdown
router.get("/getAllActiveCity/:id", async (req, res) => {
  try {
    // console.log(req.params, "req.params___req.params");

    let state: any = await getAllActiveCity(req.params);
    sendEncryptedResponse(res,  state, "Get Data successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//addCity
router.post("/addCity", tokenMiddleWare, async (req:any, res) => {
  try {
    const nameData = await checkCityName(req.body, req.params);
    if (nameData) {
      return alreadyExist(res, "This City Already Exist");
    }
    let user = req.user;
    let lastID: any = await City.findOne({
      order: [["id", "DESC"]],
      limit: 1,
    });

    lastID = lastID.get({raw:true});
    let latestID = lastID.id + 1;
    req.body = { ...req.body, id: latestID };
    let city: any = await addCity(req.body, user);


    // let city: any = await addCity(req.body, user);
    sendEncryptedResponse(res,  city, "New City is Added");

  } catch (error) {
    serverError(res, error);
  }
});

//updateCity
router.put("/updateCity/:id", tokenMiddleWare, async (req:any, res) => {
  try {
    const nameData = await checkCityName(req.body, req.params);

    let user = req.user;

    if (nameData) {
      return alreadyExist(res, "This City Already Exist");
    }

    let city: any = await updateCity(req.body, req.params, user);
    sendEncryptedResponse(res,  city, "Data updated successfully");
  } catch (error) {
    serverError(res, error);
  }
});

//get by id
router.get("/getCitybyID/:id", async (req, res) => {
  try {
    let city: any = await getCitybyID(req.params);
    sendEncryptedResponse(res,  city, "Get Data Successfully by ID");
  } catch (error) {
    serverError(res, error);
  }
});

//deleteCity
router.delete("/deleteCity/:id", async (req, res) => {
  try {
    let city: any = await deleteCity(req.params);
    sendEncryptedResponse(res,  city, "Data Deleted successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//Dump Cities
router.post("/dumpCities", async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let cities: any = await dumpAllCities(req.body, t);
    await t.commit();
    sendEncryptedResponse(res,  cities, "New Cities is Added");

  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

//All City Dropdown
router.get("/getAllCityDropdown", async (req, res) => {
  try {
    let result: any = await getAllCityDropdown();
    success(res, result, "All City dropdown");
  } catch (error) {
    serverError(res, error);
  }
});

module.exports = router;
