const { MakeQuery } = require("../../services/model-service");
import { Op } from "sequelize";
import { City, State, Country } from "../../db/core/init-control-db";

//for get city filter
export const getAllCity = (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } =
    MakeQuery({
      query: query,
      Model: City,
    });

  if (forExcel) {
    return City.findAll({
      where: modelOption,
      attributes,
      include: [
        {
          model: State,
          as: "state",
          attributes: ["id", "stateName"],
          include: [
            {
              model: Country,
              as: "country",
              attributes: ["id", "countryName"],
            },
          ],
        },
      ],
      order: orderBy,
      raw: true,
    });
  } else {
    modelOption.push({ isActive: 1 });
    return City.findAndCountAll({
      where: modelOption,
      attributes,
      include: [
        {
          model: State,
          as: "state",
          attributes: ["id", "stateName"],
          include: [
            {
              model: Country,
              as: "country",
              attributes: ["id", "countryName"],
            },
          ],
        },
      ],
      order: orderBy,
      raw: true,
      limit,
      offset,
      logging: false,
    });
  }
};

//for get all state for dropdown
export const getAllActiveCity = (params: any) => {
  return City.findAll({
    where: { stateId: params.id },
  });
};
//create city
export const addCity = async (body: any, user: any) => {
  return City.create(body);
};

//for update city
export const updateCity = (body: any, params: any, user: any) => {
  return City.update(body, {
    where: { id: params.id }});
};

//for get by id
export const getCitybyID = (params: any) => {
  return City.findOne({
    where: {
      id: params.id,
    },
    include: [
      {
        model: State,
        as: "state",
        attributes: ["id", "stateName"],
        include: [
          {
            model: Country,
            as: "country",
            attributes: ["id", "countryName"],
          },
        ],
      },
    ],
    raw: true,
  });
};

//for delete city
export const deleteCity = (params: any) => {
  return City.destroy({
    where: {
      id: params.id,
    },
  });
};

//for check unique city name
export const checkCityName = (body: any, params: any) => {
  const { cityName } = body;

  //for update time
  if (params.id) {
    return City.findOne({
      where: { cityName, id: { [Op.ne]: params.id } },
      raw: true,
    });

    // for add time
  } else {
    return City.findOne({
      where: { cityName },
      raw: true,
    });
  }
};

export const dumpAllCities = async (body: any, t: any) => {
  return City.bulkCreate(body, { transaction: t });
};

export const getAllCityDropdown = () => {
  return City.findAll();
};
