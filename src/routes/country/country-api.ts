import express from "express";
import { 
  addCountry,
  checkCountryName,
  deleteCountry,
  getAllActiveCountry,
  getAllCountry,
  getCountrybyID,
  updateCountry,
  dumpAllCountries
 } from "./country-handler";
import {
  alreadyExist,
  serverError,
  success,
} from "proses-response";
import prosesjwt from "proses-jwt";
const {tokenMiddleWare}  = prosesjwt;
const router = express.Router();
import dbInstance from "../../db/core/control-db"
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { Country } from "../../db/core/init-control-db";

//findAllCountry
router.get("/getAllCountry", async (req, res) => {
  try {
    let allCountry: any = await getAllCountry(req.query);
    sendEncryptedResponse(res,  allCountry, "Get all Countries");

  } catch (error) {
    serverError(res, error);
  }
});

//Get all countries for dropdown
router.get("/getAllActiveCountry", async (req, res) => {
  try {
   
    let country: any = await getAllActiveCountry();
    sendEncryptedResponse(res,  country, "Get Data successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//addCountry
router.post("/addCountry", async (req, res) => {
  try {
    const nameData = await checkCountryName(req.body, req.params);
    if (nameData) {
      return alreadyExist(res, "This Country Already Exist");
    }

    let lastID: any = await Country.findOne({
      order: [["id", "DESC"]],
      limit: 1,
    });

    lastID = lastID.get({raw:true});
    let latestID = lastID.id + 1;
    req.body = { ...req.body, id: latestID };
    let country: any = await addCountry(req.body);

    // let country: any = await addCountry(req.body);
    sendEncryptedResponse(res,  country, "New Country is Added");
  } catch (error) {    
    serverError(res, error);
  }
});

//updateCountry
router.put("/updateCountry/:id", async (req, res) => {
  try {
    const nameData = await checkCountryName(req.body, req.params);

    if (nameData) {
      return alreadyExist(res, "This Country Already Exist");
    }

    let country: any = await updateCountry(req.body, req.params);
    sendEncryptedResponse(res,  country, "Data updated successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//deleteCountry
router.delete("/deleteCountry/:id", tokenMiddleWare, async (req, res) => {
  try {
    let country: any = await deleteCountry(req.params);
    sendEncryptedResponse(res,  country, "Data Deleted successfully");

  } catch (error) {
    serverError(res, error);
  }
});

//get by id
router.get("/getCountrybyID/:id", async (req, res) => {
  try {
   
    let country: any = await getCountrybyID(req.params);
    sendEncryptedResponse(res,  country, "Get Data Successfully by ID");

  } catch (error) {
    serverError(res, error);
  }
});


//Dump Countries
router.post("/dumpCountries", async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let country: any = await dumpAllCountries(req.body, t);
    await t.commit();
    success(res, country, "New Country is Added");
  } catch (error) {
    await t.rollback();
    serverError(res, error);
  }
});

module.exports = router;
