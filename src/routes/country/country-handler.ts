const { MakeQuery } = require("../../services/model-service");
import { Op } from "sequelize";
import { Country } from "./country-model";
// export const getAllCountry = async () => {
//  
//     return Country.findAll({
//         where: { isActive: 1 },
//         });

// };

//for get country filter
export const getAllCountry = (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } = MakeQuery({
    query: query,
    Model: Country,
  });

  if (forExcel) {

    return Country.findAll({
      where: modelOption,
      attributes,
      order: orderBy,
      raw: true,
    });

  } else {
    modelOption.push({ isActive: 1 })
    return Country.findAndCountAll({
      where: modelOption,
      attributes,
      order: orderBy,
      raw: true,
      limit,
      offset,
    });
  }
};

//create country
export const addCountry = async (body: any) => {
  return Country.create(body);

};

//for get all country for dropdown
export const getAllActiveCountry = () => {
  return Country.findAll({ where: { isActive: 1 } });
}

//for update country
export const updateCountry = (body: any, params: any) => {
  return Country.update(body, { where: { id: params.id } });
};


//for delete country
export const deleteCountry = (params: any) => {
  return Country.destroy({
    where: {
      id: params.id,
    },
  });
};

//for get by id
export const getCountrybyID = (params: any) => {
  return Country.findOne({
    where: {
      id: params.id,
    },
  });
};

//for check unique country name
export const checkCountryName = (body: any, params: any) => {
  const { countryName } = body;

  //for update time
  if (params.id) {
    return Country.findOne({
      where: { countryName, id: { [Op.ne]: params.id } },
      raw: true,
    });

    // for add time
  } else {
    return Country.findOne({
      where: { countryName },
      raw: true,
    });
  }
}


export const dumpAllCountries = async (body: any, t: any) => {
  return Country.bulkCreate(body, { transaction: t });
};
