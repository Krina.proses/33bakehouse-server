import {
  Association,
  CreationOptional,
  DataTypes,
  HasManyGetAssociationsMixin,
  HasManySetAssociationsMixin,
  HasManyAddAssociationMixin,
  HasManyAddAssociationsMixin,
  HasManyCreateAssociationMixin,
  HasManyRemoveAssociationMixin,
  HasManyRemoveAssociationsMixin,
  HasManyHasAssociationMixin,
  HasManyHasAssociationsMixin,
  HasManyCountAssociationsMixin,
  InferCreationAttributes,
  InferAttributes,
  Model,
  NonAttribute,
  Sequelize,
} from "sequelize";
import { City } from "../city/city-model";
import { State } from "../state/state-model";
type CountryAssociations = "states" | "cities";

export class Country extends Model<any> {
  declare id: CreationOptional<number>;
  declare countryName: string;
  declare isoCode: string | null;
  declare isdCode: string | null;
  declare isActive: boolean | null;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  // Country hasMany Speaker

  // Country hasMany State
  declare states?: NonAttribute<State[]>;
  declare getStates: HasManyGetAssociationsMixin<State>;
  declare setStates: HasManySetAssociationsMixin<State, number>;
  declare addState: HasManyAddAssociationMixin<State, number>;
  declare addStates: HasManyAddAssociationsMixin<State, number>;
  declare createState: HasManyCreateAssociationMixin<State>;
  declare removeState: HasManyRemoveAssociationMixin<State, number>;
  declare removeStates: HasManyRemoveAssociationsMixin<State, number>;
  declare hasState: HasManyHasAssociationMixin<State, number>;
  declare hasStates: HasManyHasAssociationsMixin<State, number>;
  declare countStates: HasManyCountAssociationsMixin;

  // Country hasMany City
  declare cities?: NonAttribute<City[]>;
  declare getCities: HasManyGetAssociationsMixin<City>;
  declare setCities: HasManySetAssociationsMixin<City, number>;
  declare addCity: HasManyAddAssociationMixin<City, number>;
  declare addCities: HasManyAddAssociationsMixin<City, number>;
  declare createCity: HasManyCreateAssociationMixin<City>;
  declare removeCity: HasManyRemoveAssociationMixin<City, number>;
  declare removeCities: HasManyRemoveAssociationsMixin<City, number>;
  declare hasCity: HasManyHasAssociationMixin<City, number>;
  declare hasCities: HasManyHasAssociationsMixin<City, number>;
  declare countCities: HasManyCountAssociationsMixin;

  declare static associations: {
    states: Association<Country, State>;
    cities: Association<Country, City>;
  };

  static initModel(sequelize: Sequelize): typeof Country {
    Country.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true
        },
        countryName: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        isoCode: {
          type: DataTypes.STRING(255),
        },
        isdCode: {
          type: DataTypes.STRING(255),
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
      }
    );

    return Country;
  }
}
