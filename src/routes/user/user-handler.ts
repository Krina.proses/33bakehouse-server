import { Sequelize } from "sequelize/types";
const { MakeQuery } = require("../../services/model-service");
import { Op } from "sequelize";
import { Role, User, UserAddress } from "../../db/core/init-control-db";
import bcrypt from "bcryptjs";
// import { userTypesModels, userTypesTS } from "../../utils/userTypes";
// import { GLOBAL_CONSTANTS } from "../../utils/constants";

export const tenantUserByEmail$ = (email: string, instance: Sequelize) => {
  return User.findOne({
    where: { email },
    attributes: [
      "userName",
      "email",
      "phoneNo",
      "designation",
      "roleId",
      "password",
    ],
  });
};

//function to check password in db
export const checkPassword = (pass: string, hash: string) => {
  return bcrypt.compareSync(pass, hash);
};

//function to check email in db
export const getUserByEmail = (email: string) => {
  return User.findOne({ where: { email } });
};

//function a get user by id
export const getUserByid = (id: string) => {
  return User.findOne({
    where: { id },
    raw: true,
  });
};

//for get user filter
export const getAllUser = (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } =
    MakeQuery({
      query: query,
      Model: User,
    });

  let modalParam = {};

  let include = [
    {
      model: Role,
      as: "role",
      attributes: ["roleName"],
    },
  ];

  if (forExcel) {
    modalParam = {
      where: modelOption,
      attributes,
      order: orderBy,
      raw: true,
      include,
    };
  } else {
    modalParam = {
      where: modelOption,
      attributes,
      order: orderBy,
      raw: true,
      limit,
      offset,
      include,
    };
  }
  return User.findAndCountAll(modalParam);
};

// function to add user to the db
export const addUser = async (body: any, t: any) => {
  return User.create(body, { transaction: t });
};

//for update user ??
export const updateUser = (body: any, params: any, t: any) => {
  return User.update(body, { where: { id: params }, transaction: t });
};

// function to delete user from the db
export const deleteUser = (params: any) => {
  return User.destroy({
    where: {
      id: params.id,
    },
  });
};

//for check unique user name
export const checkUserData = (body: any) => {
  const { email, phoneNo, id } = body;
  //for update time
  if (id) {
    return User.findOne({
      where: {
        [Op.or]: [{ email }],
        id: { [Op.ne]: id },
      },
      raw: true,
    });

    // for add time
  } else {
    return User.findOne({
      where: { [Op.or]: [{ email }] },
      raw: true,
    });
  }
};

//for get all user for dropdown

export const UserData = (body: any) => {
  const { email, id } = body;
  return User.findOne({
    where: { email },
    attributes: ["email", "id", "userName"],
    raw: true,
  });
};

export const updateuserPassword = (body: any, id: number, t: any) => {

  return User.update(
    { password: body.newPassword },
    {
      where: {
        id,
      },
      transaction: t,
    }
  );
};

export const getUserByEmailSocialLogin = (email: string, socialLogin: any) => {
  return User.findOne({ where: { email, socialLogin } });
};

export const checkUser = (email: any) => {
  return User.findOne({
    where: {
      email,
      socialLogin: {  [Op.eq]: null },
    },
  });
};

//for creating user address
export const createaddressData = (body: any, t: any) => {
  return UserAddress.create(body, { transaction: t });
};

export const getUserAddressList = (params: any) => {
  return UserAddress.findAll({
    where: { userId: params.id },
    raw: true,
    nest: true,
  });
};
