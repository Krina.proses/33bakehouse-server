import express from "express";
import {
  addUser,
  checkPassword,
  checkUserData,
  createaddressData,
  deleteUser,
  getAllUser,
  getUserAddressList,
  getUserByEmail,
  getUserByid,
  updateUser,
  updateuserPassword,
  UserData,
} from "./user-handler";

import prosesjwt from "proses-jwt";
import {
  alreadyExist,
  other,
  serverError,
  success,
  unauthorized,
} from "proses-response";
let { tokenMiddleWare, generateToken } = prosesjwt;
import dbInstance from "../../db/core/control-db";

import { getAllActiveMenu, getRolebyID } from "../role/role-handler";
import { User } from "./user-model";
import { sendEmail } from "../../services/mailService";
import { generateRandomPassword } from "../../services/password-service";
import ErrorLogger from "../../db/core/logger/error-logger";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import { GLOBAL_CONSTANTS } from "../../utils/constants";

const router = express.Router();

//login endpoint
router.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user: any = await getUserByEmail(email);
    // check weather user exist or not
    if (!user) {
      return unauthorized(res, "Invalid credentials");
    }

    // compare pwd
    if (!checkPassword(password, user.password)) {
      return unauthorized(res, "Invalid Credentials");
    }

    let roleId = 0;
    let userTypeID = 0;

    const result: any = {
      id: user.id,
      email: user.email,
      roleId: roleId == 0 ? user.roleId : roleId,
    };
    //token
    const token = generateToken(result);

    const { password: p, ...plainUser } = user.get({ plain: true });
    let menu = await getAllActiveMenu();

    //  get the permission list
    const getPermission: any = await getRolebyID({ id: user.roleId });
    let menuwithPermission = await insertPermission(
      menu,
      JSON.parse(getPermission.permission)
    );
    let perMenu: any = await permitedMenu(
      menu,
      JSON.parse(getPermission.permission)
    );

    let data = {
      token,
      user: plainUser,
      menu: perMenu,
      flatMenu: menuwithPermission,
    };
    // success(res, data, "login successfully");
    sendEncryptedResponse(res, data, "login successfully");
  } catch (error) {
    ErrorLogger.write({ type: "login error", error });
    serverError(res, error);
  }
});

const insertPermission = (menu: any, perm: any) => {
  // filter out only permitted menu in flat menu array for searching
  let keys = Object.keys(perm);
  let tempArr: any[] = [];
  for (let item of menu) {
    let grantPerm = keys?.includes(JSON.stringify(item.id));

    if (grantPerm) {
      tempArr.push({ ...item, permission: perm?.[item.id] });
    }
  }

  return tempArr;
};

/**
 * to get all menu list with permission
 */

//findAllUser
router.get("/getAllUser", tokenMiddleWare, async (req, res) => {
  try {
    let allUser: any = await getAllUser(req.query);
    // success(res, allUser, "Got all users");
    sendEncryptedResponse(res, allUser, "Got all users");
  } catch (error) {
    ErrorLogger.write({ type: "getAllUser error", error });
    serverError(res, error);
  }
});

router.get("/userByID/:id", tokenMiddleWare, async (req, res) => {
  try {
    let user = await getUserByid(req.params.id);
    // success(res, user, "got user by id");
    sendEncryptedResponse(res, user, "got user by id");
  } catch (error) {
    ErrorLogger.write({ type: "userByID error", error });
    serverError(res, error);
  }
});

//addUser
router.post("/addUser", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let userID = req.user.id;
    req.body = { ...req.body, createdBy: userID };
    const nameData: any = await checkUserData(req.body);
    if (nameData) {
      if (nameData.email == req.body.email) {
        return alreadyExist(res, "Email already exists");
      }
    }

    let user: any = await addUser(req.body, t);

    await t.commit();
    // success(res, user, "New User is added");
    sendEncryptedResponse(res, user, "New User is added");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "addUser error", error });
    serverError(res, error);
  }
});

//updateUser
router.put("/updateUser", tokenMiddleWare, async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    const nameData: any = await checkUserData(req.body);
    const { email, phoneNo, id } = req.body;
    if (nameData) {
      if (nameData.email == req.body.email) {
        return alreadyExist(res, "Email already exists");
      }
      if (nameData.phoneNo == req.body.phoneNo) {
        return alreadyExist(res, "Phone No. already exists");
      }
    }

    let user: any = await updateUser(req.body, id, t);
    await t.commit();
    // success(res, user, "Data updated successfully");
    sendEncryptedResponse(res, user, "Data updated successfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "updateUser error", error });
    serverError(res, error);
  }
});

//deleteUser
router.delete("/deleteUser/:id", tokenMiddleWare, async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let user: any = await deleteUser(req.params);
    await t.commit();
    // success(res, user, "Data Deleted successfully");
    sendEncryptedResponse(res, user, "Data Deleted successfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "deleteUser error", error });
    serverError(res, error);
  }
});

const adjustMenu = (data: any, parent_id: any) => {
  if (!data) return;

  let tempMenu: any = [];

  data.forEach((ele: any) => {
    if (ele.parentID == parent_id) {
      let obj;

      if (parent_id == null) {
        obj = {
          type: "parent",
          id: ele.id,
          title: ele.title,
          icon: ele.icon,
          link: ele.link,
          children: adjustMenu(data, ele.id),
        };
      } else {
        obj = {
          type: "child",
          id: ele.id,
          title: ele.title,
          link: ele.link,
          icon: ele.icon,
          parentID: parent_id,
          children: [],
        };
      }

      tempMenu.push(obj);
    }
  });

  return tempMenu;
};

/** role based permited menu */
const getPermission = (singleMenu: any, permission: any) => {
  if (!singleMenu.children.length) {
    return [
      permission?.[singleMenu?.id]?.["view"],
      [],
      permission?.[singleMenu?.id],
    ];
  }

  const children = singleMenu.children
    .filter((i: any) => permission?.[i.id]?.["view"])
    .map((j: any) => ({ ...j, permission: permission?.[j.id] }));
  return [!!children.length, children, null];
};

/** return the permission based menu */
export function permitedMenu(menu: any, permission: any) {
  const MENU = adjustMenu(menu, null);
  let tempMenu: any = [];

  for (let item of MENU) {
    const [flag, children, perm] = getPermission(item, permission);
    if (flag) {
      if (perm) {
        tempMenu.push({
          ...item,
          children,
          permission: perm,
        });
      } else {
        tempMenu.push({
          ...item,
          children,
        });
      }
    }
  }

  return tempMenu;
}

/**
 *  menu - if menu/submenu hav view permission then it is in sidebar
 */
export var setPermission = function (menu: any, perm: any) {
  let d = setMenuOfViewPermission(menu, perm, false);
  return d;
  function setMenuOfViewPermission(
    menu: any,
    permission: any,
    isChild = false
  ) {
    return menu.filter((item: any) => {
      const basePerm = {
        view: false,
        add: false,
        edit: false,
        delete: false,
        disabled: false,
      };

      const singlePerm = permission ? permission[item?.id] : basePerm;

      if (item?.children?.length == 0) {
        item.permission = singlePerm;
        return true;
      } else if (item?.children?.length > 0) {
        /**if menu have children then recursive call for pemission set in child menu */
        item.children = setMenuOfViewPermission(item.children, perm, true);
        return item.children.length > 0;
      } else {
        return false;
      }
    });
  }
};

//forgot password
router.post("/forgotPassword", async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    const { email } = req.body;

    // let user = await User.findOne({
    //   where: { email },
    //   attributes: ["email", "id", "userName"],
    //   raw: true,
    // });

    let user = await UserData(req.body);

    if (!user) {
      throw unauthorized(res, "Invalid Email");
    }
    const newPWD = generateRandomPassword();

    //send email - forgot pass
    const mailData: any = {
      to: email,
      subject: "Mas Natural-New Password",
      htmlFile: "forgotPassword",
      html: `<p> Your new Password is <strong>${newPWD}</strong></p>.`,
      replacements: {
        user: {
          ...user,
          password: newPWD,
        },
      },
    };

    sendEmail(mailData);

    // update newly generated password
    const usr = await User.update(
      {
        password: newPWD,
      },
      { where: { email }, transaction: t }
    );

    // // update newly generated password
    // await User.update(
    //   { password: newPWD },
    //   { where: { id: loggedInUser?.id } }
    // );

    await t.commit();
    // success(res, user, "New Password Sent via Email");
    sendEncryptedResponse(res, user, "New Password Sent via Email");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "forgotPassword error", error });
    serverError(res, error);
  }
});
// router.post("/register", async (req, res) => {
//   let t = await dbInstance.transaction();
//   try {
//     const isExists =  await User.findOne({
//       where: {
//         email: req.body.email,
//       },
//     });

//     if(isExists) return alreadyExist(res,'User Already Exists')

//     // @ts-ignore
//     let user = await User.create({
//       name: req.body.name,
//       mobile: req.body.mobile,
//       email: req.body.email,
//       password: req.body.password,
//     });
//     await t.commit();
//     success(res, user, "User Registration Successful");
//   } catch (error) {
//     console.log("error", error);
//     await t.rollback();
//     ErrorLogger.write({ type: "user registration error", error });
//     serverError(res, error);
//   }
// });
//Update Password
router.post("/changePassword", tokenMiddleWare, async (req, res) => {
  let t = await dbInstance.transaction();
  try {
    let { oldPassword } = req.body;
    // @ts-ignore-

    let user = await User.findOne({
      // @ts-ignore-
      where: { id: req.user.id },
      attributes: ["email", "id", "password"],
      raw: true,
    });
    // @ts-ignore-
    if (!checkPassword(oldPassword, user.password)) {
      throw other(res, "Your old Password is wrong");
    }
    //@ts-ignore-
    let newValue = await updateuserPassword(req.body, req.user.id, t);
    await t.commit();
    // success(res, newValue, "Your Password has been successfully updated!!");
    sendEncryptedResponse(
      res,
      newValue,
      "Your Password has been successfully updated!!"
    );
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "changePassword error", error });
    serverError(res, error);
  }
});

////////////////  cart proses /////////

//post userAddress
router.post("/adduseraddress", async (req: any, res: any) => {
  let t = await dbInstance.transaction();
  try {

    let userAddressObj: any;

    if (req.body.userId) {
      userAddressObj = {
        userId: req.body.userId,
        title: req.body.title,
        address: req.body.address,
        cityName: req.body.cityName,
        pincode: req.body.pincode,
        isLogin: 1,
      };
    }
    
    // else {

    //   userAddressObj = {
    //     // userId: req.body.userId,
    //     name: req.body.name,
    //     mobile: req.body.mobileNo,
    //     email: req.body.email,
    //     address: req.body.address,
    //     cityName: req.body.cityName,
    //     pincode: req.body.pincode,
    //     isLogin: 0,
    //   };
    // }

    await createaddressData(userAddressObj, t);
    await t.commit();
    success(res, userAddressObj, "Address added successfuly");
  } catch (error) {
    console.log(error,"errorrrrr");
    await t.rollback();
    serverError(res, error);
  }
});

//get all user address list
router.get("/getUserAddressList/:id", async (req: any, res: any) => {
  try {
    let user = await getUserAddressList(req.params);
    success(res, user, "Get Data successfully");
  } catch (error) {
    serverError(res, error);
  }
});

module.exports = router;
