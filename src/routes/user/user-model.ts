import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  BelongsToCreateAssociationMixin,
  CreationOptional,
  DataTypes,
  InferCreationAttributes,
  InferAttributes,
  Model,
  NonAttribute,
  Sequelize,
} from "sequelize";
import { checkPassword, hashPassword } from "../../services/password-service";
import { ActivityLogs } from "../activitylogs/activitylogs-model";

import type { Role } from "../role/role-model";

type UserAssociations = "role" | "activityLogs";

export class User extends Model<
  InferAttributes<User, { omit: UserAssociations }>,
  InferCreationAttributes<User, { omit: UserAssociations }>
> {
  declare id: CreationOptional<number>;
  declare name: string;
  declare mobile: number;
  declare address: string;
  declare pincode: number;
  declare email: string | null;
  declare isActive: boolean;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;
  declare roleId: number;
  declare password: string;
  declare socialLogin: string | null;

  // User belongsTo Role
  declare role?: NonAttribute<Role>;
  declare getRole: BelongsToGetAssociationMixin<Role>;
  declare setRole: BelongsToSetAssociationMixin<Role, number>;
  declare createRole: BelongsToCreateAssociationMixin<Role>;

  declare activityLogs?: NonAttribute<ActivityLogs[]>;

  declare static associations: {
    role: Association<User, Role>;
    activityLogs: Association<User, ActivityLogs>;
  };

  //validate password
  validatePassword(this: User, userPass: string) {
    return checkPassword(userPass, this.password);
  }

  static initModel(sequelize: Sequelize): typeof User {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true,
          // defaultValue: 0,
        },
        name: {
          type: DataTypes.STRING(255),
        },
        mobile: {
          type: DataTypes.BIGINT,
        },
        email: {
          type: DataTypes.STRING(255),
          // unique: {
          //   name: "email",
          //   msg: "email must be unique",
          // },
        },
        address: {
          type: DataTypes.TEXT("long"),
        },
        pincode: {
          type: DataTypes.INTEGER,
        },
        roleId: {
          type: DataTypes.INTEGER,
        },
        password: {
          type: DataTypes.STRING(255),
          set(this: User, value: string) {
            if (!value) return;
            let hash = null;
            hash = hashPassword(value);
            this.setDataValue("password", hash);
          },
          allowNull: true,
        },
        socialLogin: {
          type: DataTypes.STRING(255),
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );

    return User;
  }
}
