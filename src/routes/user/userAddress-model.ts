import { CreationOptional, DataTypes, Model, Sequelize } from "sequelize";

export class UserAddress extends Model {
  declare id: CreationOptional<number>;
  declare userId: number;
  declare name: string;
  declare mobile: number;
  declare email: string;
  declare title: string;
  declare address: string;
  declare cityName: string;
  declare pincode: number;
  declare isLogin: boolean;
  declare isActive: boolean;
  declare createdBy: number | null;
  declare modifiedBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

  //declare association here
  declare static associations: {};

  static initModel(sequelize: Sequelize): typeof UserAddress {
    UserAddress.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true,
        },
        userId: {
          type: DataTypes.INTEGER,
        },
        name: {
          type: DataTypes.STRING(255),
        },
        mobile: {
          type: DataTypes.INTEGER,
        },
        email: {
          type: DataTypes.STRING(255),
        },
        title: {
          type: DataTypes.STRING(255),
        },
        address: {
          type: DataTypes.STRING(255),
        },
        cityName: {
          type: DataTypes.STRING(255),
        },
        pincode: {
          type: DataTypes.INTEGER,
        },
        isLogin: {
          type: DataTypes.BOOLEAN,
          defaultValue: 0,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: 1,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return UserAddress;
  }
}
