// Following Mention Module to be create
// Functionality- create, Listing, Update, soft delete
// following Fields are create title, description, Image,Status

// admin Are allow to create category, update, edit and delete
import {
  Association,
  CreationOptional,
  DataTypes,
  Model,
  Sequelize,
} from "sequelize";
// import { CombosDetails } from "../combos/combos-details-model";
import { Item } from "../item/item-model";

type CategoryAssociations = "Category";

export class Category extends Model {
  declare id: CreationOptional<number>;
  declare title: string;
  declare description: string;
  declare image: string;
  declare displayInFront: boolean;
  declare isActive: boolean;
  declare modifiedBy: number | null;
  declare createdBy: number | null;
  declare createdAt: CreationOptional<Date>;
  declare updatedAt: CreationOptional<Date>;

    declare static associations: {
        categoryItem: Association<Category, Item>;
        // categoryComboDetails: Association<Category, CombosDetails>;

    }

  static initModel(sequelize: Sequelize): typeof Category {
    Category.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
          unique: true,
        },
        title: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING(255),
        },
        image: {
          type: DataTypes.STRING(255),
        },
        displayInFront: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
        },
        createdBy: {
          type: DataTypes.INTEGER,
        },
        modifiedBy: {
          type: DataTypes.INTEGER,
        },
        createdAt: {
          type: DataTypes.DATE,
        },
        updatedAt: {
          type: DataTypes.DATE,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );
    return Category;
  }
}
