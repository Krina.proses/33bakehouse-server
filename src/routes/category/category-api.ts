import express from "express";
import { alreadyExist, serverError, success } from "proses-response";
import prosesjwt from "proses-jwt";
let { tokenMiddleWare, generateToken } = prosesjwt;
import dbInstance from "../../db/core/control-db";
import ErrorLogger from "../../db/core//logger/error-logger"
import { validate } from "../../middlewares/validation-middleware";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
import {
  addCategory,
  updateCategory,
  deleteCategory,
  getAllCategory,
  getCategoryByID,
  checkUniqueTitle,
  getAllActiveCategory,
  getAllCategories,
} from "./category-handler";
// import { Category } from "../../services/multer";
import { v_params } from "../../validations/zod-params";
import { Category } from "../../services/multer";

const router = express.Router();

router.post("/addCategory", tokenMiddleWare, Category.single("image"), async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    const image = req.file.filename;
    const body = req.body;

    let parsedFormData = JSON.parse(body.formData);
    parsedFormData.image = image;
    //newtitle comes as undefined without await
    var newTitle: any = await checkUniqueTitle(parsedFormData?.title);
    if (newTitle) {
      throw alreadyExist(res, "This Category Name is arleady exists");
    }

    let newCategory: any = await addCategory(parsedFormData, t);

    await t.commit();
    sendEncryptedResponse(res, newCategory, "New Category Added Successfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "addCategory error", error });
    serverError(res, error);
  }
});

router.put(
  "/updateCategory/:id",
  tokenMiddleWare,
  Category.single("image"),
  async (req: any, res) => {
    let t = await dbInstance.transaction();
    try {
      let body = req.body;
      let parsedFormData = JSON.parse(body.formData);
      parsedFormData.image = req.file
        ? req.file.filename
        : parsedFormData.image;

      let newTitle: any = await checkUniqueTitle(
        parsedFormData?.title,
        req.params
      );
      if (newTitle) {
        throw alreadyExist(res, "This Category Name is arleady exists");
      }

      //getting the id from the url parameteres
      let newCategory: any = await updateCategory(
        parsedFormData,
        req.params.id,
        t
      );
      await t.commit();
      sendEncryptedResponse(res, newCategory, "Category updated Successfully");
    } catch (error) {
      await t.rollback();
      ErrorLogger.write({ type: "updateCategory error", error });
      // serverError(res, error);
    }
  }
);

router.put("/deleteCategory/:id", tokenMiddleWare, async (req: any, res) => {
  let t = await dbInstance.transaction();
  try {
    let cateogry: any = await deleteCategory(req.params, t);
    await t.commit();
    sendEncryptedResponse(res, cateogry, "Data deleted succesfully");
  } catch (error) {
    await t.rollback();
    ErrorLogger.write({ type: "delete Cateogry error", error });
    serverError(res, error);
  }
})

router.get("/getAllCategory", tokenMiddleWare, async (req: any, res) => {
  try {
    let allCategories: any = await getAllCategory(req.query);
    sendEncryptedResponse(res, allCategories, "got all categories");
  } catch (error) {
    ErrorLogger.write({ type: "getAllCateogry error", error });
    serverError(res, Error);
  }
});

//get all categories for mobile
router.get('/getAllCategories', async (req: any, res) => {
  try {
    let allCategories: any = await getAllCategories();
    // success(res, allCategories, "got all categories");
    sendEncryptedResponse(res, allCategories, "got all categories");
  } catch (error) {
    ErrorLogger.write({ type: "getAllCateogry error", error });
    serverError(res, Error);
  }
})

router.get("/getCategoryByID/:id", tokenMiddleWare, validate({ params: v_params }), async (req: any, res) => {
  try {
    let category: any = await getCategoryByID(req.params.id);
    // success(res, category, "got Category by ID");
    sendEncryptedResponse(res, category, "got Category by ID");
  } catch (error) {
    ErrorLogger.write({ type: "getCategoryByID error", error });
    serverError(res, error);
  }
});

router.get('/getCategoryDropDown', tokenMiddleWare, async (req: any, res) => {
  try {
    let allCategories: any = await getAllActiveCategory();
    // success(res, allCategories, "got all categories");
    sendEncryptedResponse(res, allCategories, "got all categories");
  } catch (error) {
    ErrorLogger.write({ type: "getAllCateogry error", error });
    serverError(res, Error);
  }
})



module.exports = router;
