import { Category } from "./category-model";
import { Op, Transaction } from "sequelize";
// const { MakeQuery } = require("../../services/model-service");
import { MakeQuery } from "../../services/model-service";
import { Item } from "../item/item-model";

export const addCategory = async (body: any, t: Transaction) => {
  return Category.create(body, { transaction: t });
};

export const updateCategory = async (
  body: any,
  params: any,
  t: Transaction
) => {
  return Category.update(body, { where: { id: params }, transaction: t });
};

//soft delete
//is the same as update because we are only doing soft delete
// export const deleteCategory = (params: any, t: Transaction) => {
//   return Category.update({isDeleted: true}, { where: { id: params.id }, transaction: t });
// };

export const deleteCategory = (params: any, t: Transaction) => {
  return Category.destroy({ where: { id: params.id }, transaction: t });
};

export const getCategoryByID = (id: number) => {
  return Category.findOne({
    where: { id },
  });
};

export const getAllCategory = async (query: any) => {
  const { limit, offset, modelOption, orderBy, attributes, forExcel } =
    MakeQuery({
      query: query,
      Model: Category,
    });

  let modalParam = {};
  if (forExcel) {
    modalParam = {
      where: modelOption,
      attributes,
      order: orderBy,
      raw: true,
    };
    return Category.findAll(modalParam);
  } else {
    modalParam = {
      where: modelOption,
      attributes,
      order: orderBy,
      limit,
      raw: true,
      offset,
    };
    return Category.findAndCountAll(modalParam);
  }
};

//get all categories for app
export const getAllCategories = () => {
  // let modelParam = {
  //   where: {
  //     isDeleted: false,
  //   },
  //   raw: true,
  // };
  return Category.findAll({
    where: { displayInFront: true },
  });
};

//check for unique title

export const checkUniqueTitle = (title: any, params?: any) => {
  //for update time
  if (params) {
    return Category.findOne({
      where: { title, id: { [Op.ne]: params.id }, isActive: 1 },
    });

    // for add time
  } else {
    return Category.findOne({
      where: { title, isActive: 1 },
    });
  }
};

//for get all Category for dropdown
export const getAllActiveCategory = () => {
  return Category.findAll({
    where: { isActive: 1 },
  });
};
