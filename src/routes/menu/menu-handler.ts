import { Sequelize } from "sequelize/types";
import { Menu } from "./menu-model";

export const getMenu$ = async (instance: Sequelize) => {
  try {
    return Menu.findAll({
      where: { isActive: 1 },
    });
  } catch (error) {
    return error;
  }
};
