import {
  CreationOptional,
  DataTypes,
  InferCreationAttributes,
  InferAttributes,
  Model,
  Sequelize,
} from "sequelize";

export class Menu extends Model<
  InferAttributes<Menu>,
  InferCreationAttributes<Menu>
> {
  declare id: CreationOptional<number>;
  declare parentID: number | null;
  declare title: string;
  declare icon: string;
  declare link: string | null;
  declare isActive: number | null;
  declare sequenceNumber: number | null;

  static initModel(sequelize: Sequelize): typeof Menu {
    Menu.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          primaryKey: true,
          autoIncrement: true,
          allowNull: false,
        },
        parentID: {
          type: DataTypes.INTEGER,
          defaultValue: null,
        },
        title: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        link: {
          type: DataTypes.STRING,
          defaultValue: null,
        },
        icon: {
          type: DataTypes.STRING,
          defaultValue: null,
        },
        isActive: {
          type: DataTypes.SMALLINT,
          defaultValue: null,
        },
        sequenceNumber: {
          type: DataTypes.SMALLINT,
          defaultValue: null,
        },
      },
      {
        sequelize,
        freezeTableName: true,
      }
    );

    return Menu;
  }
}
