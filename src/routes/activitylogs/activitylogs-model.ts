import {
    Association,
    CreationOptional,
    DataTypes,
    Model,
    Sequelize,
    TextDataType,
    NonAttribute
  } from 'sequelize'

  import { User } from '../user/user-model'

  type ActivityLogsAssociations = 'user'
  
  export class ActivityLogs extends Model {
    declare id: CreationOptional<number>
    declare userId: Number
    declare oldData: string
    declare newData: string
    declare activityType: string
    declare pageName: string
    declare createdBy: Date | null
    declare modifiedBy: number | null
    declare createdAt: CreationOptional<Date>
    declare updatedAt: CreationOptional<Date>

    declare user?: NonAttribute<User>

    declare static associations: {
      user: Association<ActivityLogs, User>;
    }
  
    static initModel(sequelize: Sequelize): typeof ActivityLogs {
      ActivityLogs.init({
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          unique: true
        },
        userId: {
          type: DataTypes.INTEGER   
        },
        oldData: {
          type: DataTypes.TEXT      
        },
        newData: {
          type: DataTypes.TEXT      
        },
        activityType: {
          type: DataTypes.STRING      
        },
        createdBy: {
          type: DataTypes.INTEGER
        },
        modifiedBy: {
          type: DataTypes.INTEGER
        },
        createdAt: {
          type: DataTypes.DATE
        },
        updatedAt: {
          type: DataTypes.DATE
        }
      }, {
        sequelize
      })
      
      return ActivityLogs
    }
  }
  