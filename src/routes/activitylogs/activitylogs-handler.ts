import { ActivityLogs } from "./activitylogs-model";
import { User } from "../user/user-model";
import sequelize from "sequelize";

const { MakeQuery } = require("../../services/model-service");


//get all ActivityLogs filterwise
export const getActivityLogsFilterWise = (query: any) => {
    const { limit,id, offset, modelOption, orderBy, attributes, forExcel } = MakeQuery({
      query: query,
      Model: ActivityLogs,
      orderSetting: {
        NoDefault: false,
      },
    });
    if (id) {
      modelOption.push({ id });
      return ActivityLogs.findOne({
        where: modelOption,
        attributes: attributes,
        raw: true
      });
    }else if (forExcel) { 
      return ActivityLogs.findAll({
        where: modelOption,
        attributes,
        order: [["id", "desc"]],
        raw: true,
      }); 
    }else{
        return ActivityLogs.findAndCountAll({
        // where:
          attributes,
          include: [
            {
              model: User,
              attributes:['id', 'email']
            }
          ],
          raw: true,
          offset,
          limit,
          logging: false,
          order: [["id", "desc"]],
      })
  }
  }