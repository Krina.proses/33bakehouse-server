import express from "express";
import { serverError, success } from "proses-response";
import { getActivityLogsFilterWise } from "./activitylogs-handler";
import { sendEncryptedResponse } from "../../services/encryptResponse-service";
const router = express.Router();

//list-api
router.get("/getActivityLogsFilterWise", async (req, res) => {
    try {      
      let allActivityLogs: any = await getActivityLogsFilterWise(req.query);
      // success(res, allActivityLogs, "Get all ActivityLogss");
      sendEncryptedResponse(res,  allActivityLogs, "Get all ActivityLogss");

    } catch (error) {
      serverError(res, error);
    }
  });
  module.exports = router;