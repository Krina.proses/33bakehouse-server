import { Application } from "express";

export default (app: Application) => {
  //for copy-paste
  // app.use("", require(""))
  app.use("/user", require("./user/user-api"));
  app.use("/permission", require("./user/permission.api"))
  app.use("/role", require("./role/role-api"))
  app.use("/menu", require("./menu/menu-api"))
  app.use("/activitylogs", require("./activitylogs/activitylogs-api"))
  app.use("/country", require("./country/country-api"))
  app.use("/state", require("./state/state-api"))
  app.use("/city", require("./city/city-api"))
  app.use("/category", require("./category/category-api"));
  // app.use("/attribute", require("./attribute/attribute-api"));
  app.use("/item", require("./item/item-api"));
  app.use("/order", require("./item-order/itemOrder-api"));
  app.use("/customerApi", require("./customerApi/customerApi-api"));
  app.use("/cart", require("./cart/cart-api"));
};
